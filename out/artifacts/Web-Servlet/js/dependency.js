﻿/* Dependency */
let table = document.getElementById('table'); // Input table.
let tableRow = table.getElementsByTagName('td');   // Init point of table.
let option = document.getElementById('buttons-option').querySelectorAll('input');

function createElement(_id, array, tableId, attribute, style) {
  let row = document.createElement('tr');
  if(_id !== 0) row.id = _id;

  // Set attribute for row.
  if (typeof attribute != 'undefined' && 'object' == typeof attribute)
    attribute.forEach(item => setAttribute(row, item));

  // Create template element.
  for (let i in array) {
    let element = document.createElement('td');
    if ('object' == typeof array[i]) validateStyle(element, array[i], 1);
    else append(element, array[i]);
    row.appendChild(element);
  }

  if(typeof style != 'undefined') row.style = style;
  if(typeof tableId == 'undefined') table.appendChild(row);
  else document.getElementById(tableId).appendChild(row);

  function validateStyle(element, value, index) {
    if('object' == typeof value[0]) {
      if('object' == typeof value[0][1] && index != 1)
        value[0].forEach(item => validateStyle(element, [item], 2));
      else {
        let subElemtent = document.createElement(value[0][1]);
        validateStyle(subElemtent, value[0], 2)
        subElemtent.value = value[0][0];
        element.appendChild(subElemtent);
      }
    } else append(element, value[0]);

    for (let i = index; i < value.length; i++)
      if ('object' == typeof value[i]) setAttribute(element, value[i]);
      else element.style = value[i];
  }

  function append(element, value) {
    element.innerHTML = value;
  }

  function setAttribute(element, array) {
    if(array.length == 2) element.setAttribute(array[0], array[1]);
    else array[0](element);
  }
}

function clearTable(tableId) {
  let tempTable;
  if(typeof tableId == 'undefined') tempTable = table;
  else tempTable = tableId;

  while (tempTable.hasChildNodes())
    tempTable.removeChild(tempTable.lastChild);
}

function setDisplay(id, display) {
  let element = document.getElementById(id);
  element.style.display = display;
}

/*  Button  */
function freezeButton(name, boolean) {
  document.getElementById('buttons').querySelectorAll('input').forEach(item => {
    if(item.name === name) item.disabled = boolean;
  });
}

function clearButton() {
  document.getElementById('buttons').querySelectorAll('input').forEach(item => {
    item.disabled = false;
  });
}

/*  input   */
function freezeInput(id, arrayType, boolean) {
  let form = document.getElementById(id);
  if('object' == typeof arrayType) arrayType.forEach(type => {
      form.querySelectorAll('input[type=' + type + ']').forEach(input => {
        input.disabled = boolean;
      });
    });
  else freezeInput(id, [arrayType], boolean);
}

/*  Format conversion  */
function toDate(string) {
  return new Date(string).toLocaleDateString('ru-RU');
}

/*  Sort  */
function sortTable(value, data, cTitle, fForm, tableId) {
  if(typeof tableId != 'undefined') clearTable(document.getElementById(tableId));
  cTitle();
  let countCreate = 0;
  data.forEach(item => {
      for (var valueItem in item)
        if(item[valueItem].indexOf(value) !== -1) {
          fForm(item);
          countCreate++;
          break;
        }
  });
  if(!countCreate) createElement(0, [
    ["Список пуст.", "text-align: center;",
    ["colspan", Object.keys(data[0]).length - 1]]
  ], tableId);
}

/* Sort data */
function sortDataForColumn(array, column, typeSort) {
  return array.slice().sort((a, b) => (
      a[column] > b[column] ? typeSort * 1 : typeSort * (-1))
  );
}

function sortDataForDate(array, typeSort) {
  return array.slice().sort((a, b) => (
      new Date(a.date) > new Date(b.date) ? typeSort * 1 : typeSort * (-1))
  );
}
