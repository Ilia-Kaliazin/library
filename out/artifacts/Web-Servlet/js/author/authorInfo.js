function onAuthorInfo(id) {
  let tableId = 'authorInfoTable';
  clearTable(document.getElementById(tableId));
  authors.forEach(author => {
    if(author._id == id) {
      createElement(0,
      [
        "Имя автора",
        author.name
      ], tableId);

      createElement(0,
      [
        "Отчество автора",
        author.secondName
      ], tableId);

      createElement(0,
      [
        "Фамилия автора",
        author.lastName
      ], tableId);

      createElement(0,
      [
        "Дата рождения",
        toDate(author.dob)
      ], tableId);

      createElement(0,
      [
        [
          "Написанных книг:",
          "font-weight: bold;"
        ],
        (function() {
          let count = 0;
          books.forEach(book => {
            if(book.authorName == author.name &&
              book.authorLastName == author.lastName &&
              book.authorDob == author.dob) count++;
          });
          return count;
        }())
      ], tableId);

      createElement(0,
      [
        [
          [
            [
              [
                "Удалить",
                "input",
                ["type", "submit"]
              ],
              [
                author.name,
                "input",
                ["type", "hidden"],
                ["name", "authorName"]
              ],
              [
                author.lastName,
                "input",
                ["type", "hidden"],
                ["name", "authorLastName"]
              ],
              [
                author.secondName,
                "input",
                ["type", "hidden"],
                ["name", "authorSecondName"]
              ],
              [
                author.dob,
                "input",
                ["type", "hidden"],
                ["name", "authorDob"]
              ]
            ]
          ]
        ]
      ], tableId);
    }
  });

  setDisplay('authorInfo', 'block');
}
