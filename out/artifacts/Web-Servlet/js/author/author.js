function fillFormAuthors(item) {
  createElement(item['_id'],
  [
    item['name'],
    item['lastName'],
    item['secondName'],
    toDate(item['dob'])
  ], "table", [["onclick", "onAuthorInfo(id)"]]);
}

function createTitleAuthors() {
  createElement(0,
  [
    "Имя",
    "Фамилия",
    "Отчество",
    "Дата рождения"
  ]);
}

function loadAuthors() {
  createTitleAuthors();
  authors.forEach(item => fillFormAuthors(item));
  if(!authors.length) createElement(0, [
    ["Список пуст.", "text-align: center;",
      ["colspan", 4]]
  ]);
}
