function fillFormBook(item) {
  createElement(item['_id'],
  [
    item['bookName'],
    item['releaseYear'],
    item['publisher'],
    item['authorName']
  ], "table", [["onclick", "onBookInfo(id)"]]);
}

function createTitleBook() {
  createElement(0,
  [
    "Название книги",
    "Год",
    "Опубликован",
    "Автор"
  ]);
}

function loadBooks() {
  createTitleBook();
  books.forEach(item => fillFormBook(item));
  if(!books.length) createElement(0, [
    ["Список пуст.", "text-align: center;",
      ["colspan", 4]]
  ]);
}
