function onBookInfo(id) {
  let tableId = 'bookInfoTable';
  clearTable(document.getElementById(tableId));
  books.forEach(book => {
    if(book._id == id) {
      createElement(0,
      [
        "Название книги",
        book.bookName
      ], tableId);

      createElement(0,
      [
        "Год выпуска",
        book.releaseYear
      ], tableId);

      createElement(0,
      [
        "Кол-во страниц",
        book.pageCount
      ], tableId);

      createElement(0,
      [
        "Опубликован",
        book.publisher
      ], tableId);

      createElement(0,
      [
        "ISBN",
        book.isbn
      ], tableId);

      createElement(0,
      [
        [
          "Информация о авторе",
          "text-align: center; font-weight: bold;",
          ["colspan", "2"]
        ]
      ], tableId);

      createElement(0,
      [
        "Имя автора",
        book.authorName
      ], tableId);

      createElement(0,
      [
        "Отчество автора",
        book.authorSecondName
      ], tableId);

      createElement(0,
      [
        "Фамилия автора",
        book.authorLastName
      ], tableId);

      createElement(0,
      [
        "Дата рождения",
        toDate(book.authorDob)
      ], tableId);

      createElement('buttons',
      [
        [
          [
            "Добавить закладку",
            "input",
            ["type", "button"],
            ["onclick", "showContextBox(this,'" + book.isbn + "')"]
          ]
        ],
        [[
          [
            [
              "Удалить",
              "input",
              ["type", "submit"]
            ],
            [
              book.isbn,
              "input",
              ["type", "hidden"],
              ["name", "isbn"]
            ]
          ], "form", ["action", "home/deletionBook"], ["method", "post"]
        ]]
      ], tableId);
    }
  });

  setDisplay('bookInfo', 'block');
}
