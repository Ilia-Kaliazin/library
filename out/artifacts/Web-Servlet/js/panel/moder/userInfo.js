/*
createElement(1,
  [[[[["1", "p", "color: red"], ["2", "span", "color: green"]], "form"], "background-color: yellow"], "3"]
  , tableId
);
*/
function onUserInfo(id) {
    let tableId = 'userInfoTable';
    clearTable(document.getElementById(tableId));
    users.forEach(user => {
        if(user._id == id) {
            createElement(0,
                [
                    "Логин",
                    user.login
                ], tableId);

            createElement(0,
                [
                    "Роль",
                    user.role
                ], tableId);

            createElement(0,
                [
                    "Блокировка",
                    user.isbanned
                ], tableId);

            createElement(0,
                [
                    [
                        [
                            [
                                [
                                    (function() {
                                        if(user.isbanned) return "Разблокировать";
                                        else return "Заблокировать";
                                    }()),
                                    "input",
                                    ["type", "submit"],
                                    [function(element) {
                                        if(user.role == "admin")
                                            element.disabled = true;
                                    }]
                                ],
                                [
                                    user.login,
                                    "input",
                                    ["type", "hidden"],
                                    ["name", "login"]
                                ],
                                (function() {
                                    if(user.isbanned)
                                        return [
                                            "0",
                                            "input",
                                            ["type", "hidden"],
                                            ["name", "point"]
                                        ];
                                    else
                                        return [
                                            "1",
                                            "input",
                                            ["type", "hidden"],
                                            ["name", "point"]
                                        ];
                                }())
                            ],
                            "form",
                            ["action", "moderPanel/updateBan"],
                            ["method", "post"]
                        ]
                    ]
                ], tableId);
        }
    });

    setDisplay('userInfo', 'block');
}
