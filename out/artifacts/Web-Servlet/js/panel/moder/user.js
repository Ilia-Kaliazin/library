function getColorRole(role) {
    if ("moder" == role)
        return "background-color: #7fffb0b9";
    else if ("admin" == role)
        return "background-color: #8a2be299";
}

function fillFormUser(user) {
    createElement(user['_id'],
        [
            user['login'],
            user['role'],
            user['isbanned']
        ], "table", [["onclick", "onUserInfo(id)"]],
        getColorRole(user['role']));
}

function createTitleUser() {
    createElement(0,
        [
            "Логин",
            "Роль",
            "Блокировка"
        ]);
}

(function loadUser() {
    createTitleUser();
    sortDataForColumn(users, 'role', 1).forEach(item => fillFormUser(item));
    if(!users.length) createElement(0, [
        ["Список пуст.", "text-align: center;",
            ["colspan", 3]]
    ]);
}())
