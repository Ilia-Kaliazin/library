/*
createElement(1,
  [[[[["1", "p", "color: red"], ["2", "span", "color: green"]], "form"], "background-color: yellow"], "3"]
  , tableId
);
*/
function onUserInfo(id) {
  let tableId = 'userInfoTable';
  clearTable(document.getElementById(tableId));
  users.forEach(user => {
    if(user._id == id) {
      createElement(0,
      [
        "Логин",
        user.login
      ], tableId);

      createElement(0,
      [
        "Роль",
        user.role
      ], tableId);

      createElement(0,
      [
        "Блокировка",
        user.isbanned
      ], tableId);

      createElement(0,
      [
        [
          [
            [
              [
                "Обновить роль до",
                "input",
                ["type", "submit"]
              ],
              [
                user.login,
                "input",
                ["type", "hidden"],
                ["name", "login"]
              ],
              [
                [
                  [
                    "user",
                    "option",
                    [function(element) {
                      if(user.role == "user")
                        element.disabled = true;
                    }]
                  ],
                  [
                    "moder",
                    "option",
                    [function(element) {
                      if(user.role == "moder")
                        element.disabled = true;
                    }]
                  ],
                  [
                    "admin",
                    "option",
                    [function(element) {
                      if(user.role == "admin")
                        element.disabled = true;
                    }]
                  ]
                ],
                "select",
                ["name", "role"],
                ["required", true]
              ]
            ],
            "form",
            ["action", "adminPanel/updateRole"],
            ["method", "post"]
          ],
          ["colspan", "2"]
        ]
      ], tableId);

      createElement(0,
          [
            [
              [
                [
                  [
                    "Перейти в аккаунт",
                    "input",
                    ["type", "submit"],
                    [function(element) {
                      if(user.role == "admin")
                        element.disabled = true;
                    }]
                  ],
                  [
                    user.login,
                    "input",
                    ["type", "hidden"],
                    ["name", "login"]
                  ]
                ],
                "form",
                ["action", "adminPanel/loginOn"],
                ["method", "post"]
              ]
            ]
          ], tableId);

      createElement(0,
      [
        [
          [
            [
              [
                "Загрузить логи",
                "input",
                ["type", "submit"],
                [function (element) {
                  if(selectedLogin == user.login)
                    element.disabled = true;
                }]
              ],
              [
                user.login,
                "input",
                ["type", "hidden"],
                ["name", "login"]
              ]
            ],
            "form",
            ["action", "adminPanel/updateLogs"],
            ["method", "post"]
          ]
        ]
      ], tableId);

      createElement(0,
      [
        [
          [
            [
              [
                (function() {
                  if(user.isbanned) return "Разблокировать";
                  else return "Заблокировать";
                }()),
                "input",
                ["type", "submit"]
              ],
              [
                user.login,
                "input",
                ["type", "hidden"],
                ["name", "login"]
              ],
              (function() {
                if(user.isbanned)
                  return [
                      "0",
                      "input",
                      ["type", "hidden"],
                      ["name", "point"]
                    ];
                else
                  return [
                    "1",
                    "input",
                    ["type", "hidden"],
                    ["name", "point"]
                  ];
              }())
            ],
            "form",
            ["action", "adminPanel/updateBan"],
            ["method", "post"]
          ]
        ]
      ], tableId);
    }
  });

  setDisplay('userInfo', 'block');
}
