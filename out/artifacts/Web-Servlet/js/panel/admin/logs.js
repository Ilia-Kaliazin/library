function fillFormLog(item) {
  createElement(item['_id'],
  [
    item['table'],
    item['field']
  ], "table-logs",[["onclick", "onLogInfo(id)"]]);
}

function createTitleLogs() {
  createElement(0,
  [
    "Таблица",
    "Поле"
  ], "table-logs");
}

(function loadLogs() {
  createTitleLogs();
  sortDataForDate(logs, -1).forEach(item => fillFormLog(item));
  if(!logs.length) createElement(0, [
    ["Список пуст.", "text-align: center;",
      ["colspan", 2]]
  ], "table-logs");
}())
