function onLogInfo(id) {
    let tableId = 'logInfoTable';
    clearTable(document.getElementById(tableId));
    logs.forEach(log => {
        if(log._id == id) {
            createElement(0,
                [
                    "Таблица",
                    log.table
                ], tableId);

            createElement(0,
                [
                    "Поле",
                    log.field
                ], tableId);

            createElement(0,
                [
                    "Тип изменения",
                    log.type
                ], tableId);

            createElement(0,
                [
                    "Дата изменения",
                    toDate(log.date)
                ], tableId);
        }
    });

    setDisplay('logInfo', 'block');
}