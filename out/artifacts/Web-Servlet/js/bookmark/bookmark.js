function fillFormBookmark(item) {
  createElement(item['_id'],
  [
    item['bookName'],
    item['releaseYear'],
    item['authorName'],
    item['page']
  ], "table", [["onclick", "onBookmarkInfo(id)"]]);
}

function createTitleBookmark() {
  createElement(0,
  [
    "Название книги",
    "Год",
    "Автор",
    "Страница"
  ]);
}

function loadBookmark(data) {
  createTitleBookmark();
  bookmarks.forEach(item => fillFormBookmark(item));
  if(!bookmarks.length) createElement(0, [
    ["Список пуст.", "text-align: center;",
      ["colspan", 4]]
  ]);
}
