﻿ function onBookmarkInfo(id) {
  let tableId = 'bookmarkInfoTable';
  clearTable(document.getElementById(tableId));
  bookmarks.forEach(bookmark => {
    if(bookmark._id == id) {
      createElement(0,
      [
        "Страница",
        bookmark.page
      ], tableId);

      createElement(0,
      [
        [
          "Информация о книге",
          "text-align: center; font-weight: bold;",
          ["colspan", "2"]
        ]
      ], tableId);

      createElement(0,
      [
        "Название книги",
        bookmark.bookName
      ], tableId);

      createElement(0,
      [
        "Год выпуска",
        bookmark.releaseYear
      ], tableId);

      createElement(0,
      [
        "Имя автора",
        bookmark.authorName
      ], tableId);

      createElement(0,
      [
        "Отчество автора",
        bookmark.authorSecondName
      ], tableId);

      createElement(0,
      [
        "Фамилия автора",
        bookmark.authorLastName
      ], tableId);

      createElement(0,
      [
        "Дата рождения",
        toDate(bookmark.authorDob)
      ], tableId);

      createElement(0,
      [
        [
          [
            [[
              "Удалить",
              "input",
              ["type", "submit"]
            ],
            [
              id,
              "input",
              ["type", "hidden"],
              ["name", "uuid"]
            ]]
          ]
        ]
      ], tableId);
    }
  });

  setDisplay('bookmarkInfo', 'block');
}
