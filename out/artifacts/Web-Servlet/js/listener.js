(function () {
  onButton("books");
}())

function onButton(name) {
  clearTable();
  clearButton();
  (function() {
    switch (name) {
      case "books": loadBooks(); break;
      case "authors": loadAuthors(); break;
      case "bookmarks": loadBookmark(); break;
      default:
        name = "book";
        loadBooks(); break;
    }
  }());
  freezeButton(name, true);
  option.forEach(item => item.name = name);
}

function onSortData(name, value) {
  clearTable();
  if(value.length >= 1) {
    switch (name) {
      case "books":
        sortTable(value, books, createTitleBook, fillFormBook);
        break;
      case "authors":
        sortTable(value, authors, createTitleAuthors, fillFormAuthors);
        break;
      case "bookmarks":
        sortTable(value, bookmarks, createTitleBookmark, fillFormBookmark);
        break;
    }
  } else onButton(name);
}

function showContextBox(button, isbn) {
  button.disabled = true;
  let tableId = 'bookInfoTable';
  let table = document.getElementById(tableId);

  createElement(0,
  [
    [
      [[
        [
          "",
          "input",
          ["type", "text"],
          ["name", "page"],
          ["required", true],
          ["placeholder", "Номер страницы"],
          "width: 99%"
        ],
        [
          isbn,
          "input",
          ["type", "hidden"],
          ["name", "isbn"]
        ],
      ]],
      ["colspan", "2"]
    ]
  ], tableId);

  createElement(0,
  [
    [["Добавить",
    "input",
    ["type", "submit"],
    "width: 100%"], ["colspan", "2"]]
  ], tableId);
}
