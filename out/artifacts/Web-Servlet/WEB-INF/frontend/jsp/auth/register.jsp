<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="ru" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Library | Register</title>
    <link rel="stylesheet" href="css/login.css">
</head>
<body>
<section>
    <div class="form-login box-font">
        <p>Регистрация в системе</p>
        <form action="register" method="post" onsubmit="return validatePassword(this)">
            <input type="text" required="true" maxlength="20" name="login" placeholder="Логин">
            <input type="password" required="true" maxlength="20" name="password" placeholder="Пароль">
            <input type="password" required="true" maxlength="20" name="confirm-password" placeholder="Пароль">
            <input type="submit" value="Зарегистрироваться">
        </form>
    </div>
</section>
<script>
    function validatePassword(form) {
        let password = null;

        for (let input of form) {
            if(input.type === "password" && password === null) password = input.value;
            else if(input.type === "password")
                if(password === input.value) return true;
                else {
                    input.value = null;
                    return false;
                }
        }
    }
</script>
</body>
</html>
