<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="ru" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Library | Login</title>
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="css/login-context-fixed.css">
  </head>
  <body>
    <jsp:include page="../../form/contextMsg.jsp"/>

    <section>
      <div class="form-login box-font">
        <p>Вход в систему</p>
        <form action="login" method="post">
          <input type="text" required="true" name="login" maxlength="20" placeholder="Логин">
          <input type="password" required="true" name="password" maxlength="20" placeholder="Пароль">
          <input type="submit" value="Войти">
          <input type="button" value="Зарегистрироваться" onClick="location.href='register'">
        </form>
      </div>
    </section>
    <script src="js/listenerContext.js" charset="utf-8"></script>
  </body>
</html>
