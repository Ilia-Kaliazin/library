<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="ru" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>Library | Moder panel</title>
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="css/panelModer.css">
    <link rel="stylesheet" href="css/home-context-fixed.css">
</head>
<body>
<section class="container">
    <jsp:include page="../../form/headerPanel.jsp"/>

    <div class="container-hidden">
        <table id="buttons-option">
            <tr>
                <td><input type="button" onclick="setDisplay('addUser', 'block')" value="Добавить пользователя"></td>
            </tr>
        </table>
    </div>

    <section class="container-user table-box box-font">
        <div class="container-hidden">
            <table id="table"></table>
        </div>
    </section>
</section>

<section>
    <section id="addUser" class="form-context-fixed" style="display: none;">
        <jsp:include page="../../form/contextAddUser.jsp"/>
    </section>

    <section id="userInfo" class="form-context-fixed" style="display: none;">
        <jsp:include page="../../form/contextInfoUser.jsp"/>
    </section>
</section>

<script charset="utf-8">
    var selectedLogin = "${selectedLogin}";
    var users = [${users}];
</script>
<script src="js/dependency.js" charset="utf-8"></script>
<script src="js/panel/moder/user.js" charset="utf-8"></script>
<script src="js/panel/moder/userInfo.js" charset="utf-8"></script>
</body>
</html>