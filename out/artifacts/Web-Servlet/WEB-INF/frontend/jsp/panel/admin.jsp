<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="ru" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Library | Admin panel</title>
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="css/panelAdmin.css">
    <link rel="stylesheet" href="css/home-context-fixed.css">
  </head>
  <body>
    <section class="container">
      <jsp:include page="../../form/headerPanel.jsp"/>


      <div class="container-hidden">
        <table id="buttons-option">
          <tr>
            <td><input type="button" onclick="setDisplay('addUser', 'block')" value="Добавить"></td>
            <td>
              <div class="data-sort box-font">
                <p>Поиск по таблице</p>
                <input type="text" oninput="sortTable(this.value, sortDataForDate(logs, -1), createTitleLogs, fillFormLog, 'table-logs')" placeholder="Введите значения">
              </div>
            </td>
          </tr>
        </table>
      </div>

      <section style="display: inline-grid;">
        <section class="container-selected-user table-box box-font">
          <div class="container-hidden">
            <table id="table-selected-user"></table>
          </div>
        </section>

        <section class="container-user table-box box-font">
          <div class="container-hidden">
            <table id="table"></table>
          </div>
        </section>
      </section>

      <section class="container-logs table-box box-font">
        <div class="container-hidden">
          <table id="table-logs"></table>
        </div>
      </section>
    </section>

    <section>
      <section id="addUser" class="form-context-fixed" style="display: none;">
        <jsp:include page="../../form/contextAddUser.jsp"/>
      </section>

      <section id="userInfo" class="form-context-fixed" style="display: none;">
        <jsp:include page="../../form/contextInfoUser.jsp"/>
      </section>

      <section id="logInfo" class="form-context-fixed" style="display: none;">
        <jsp:include page="../../form/contextInfoLog.jsp"/>
      </section>
    </section>
    <script charset="utf-8">
      var users = [${users}];
      var selectedLogin = "${selectedLogin}";
      var logs = [${logs}];
    </script>
    <script src="js/dependency.js" charset="utf-8"></script>
    <script src="js/panel/admin/user.js" charset="utf-8"></script>
    <script src="js/panel/admin/userInfo.js" charset="utf-8"></script>
    <script src="js/panel/admin/logs.js" charset="utf-8"></script>
    <script src="js/panel/admin/logInfo.js" charset="utf-8"></script>
  </body>
</html>
