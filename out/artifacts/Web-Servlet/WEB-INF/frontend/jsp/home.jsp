<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="ru" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Library | Home</title>
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="css/home-context-fixed.css">
  </head>
  <body>
    <section class="container">
      <header id="header" class="box-font">
        <span>${user.role.name}</span>
        <p>${user.login}</p>
        <c:choose>
          <c:when test="${loginOn}">
            <form action="logoutOn" method="post">
              <input type="submit" value="Выйти из аккаунта">
            </form>
          </c:when>

          <c:otherwise>
            <form action="logout" method="post">
              <input type="submit" value="Выйти">
            </form>
          </c:otherwise>
        </c:choose>

        <c:choose>
          <c:when test = "${isModer}">
            <jsp:include page="../form/buttonModer.jsp"/>
          </c:when>

          <c:when test = "${isAdmin}">
            <jsp:include page="../form/buttonAdmin.jsp"/>
          </c:when>
        </c:choose>
      </header>
      
      <div class="container-hidden">
        <table id="buttons">
          <tr>
            <td><input type="button" name="books" onclick="onButton(name)" value="Книги"></td>
            <td><input type="button" name="authors" onclick="onButton(name)" value="Авторы"></td>
            <td><input type="button" name="bookmarks" onclick="onButton(name)" value="Закладки"></td>
          </tr>
        </table>
      </div>

      <div class="container-hidden">
        <table id="buttons-option">
          <tr>
            <td><input type="button" name="books" onclick="setDisplay(name, 'block')" value="Добавить"></td>
            <td>
              <div class="data-sort box-font">
                <p>Поиск по таблице</p>
                <input type="text" name="books" oninput="onSortData(name, this.value)" placeholder="Введите значения">
              </div>
            </td>
          </tr>
        </table>
      </div>

      <section class="table-box box-font">
        <div class="container-hidden">
          <table id="table">
          </table>
        </div>
      </section>
    </section>

    <section>
      <section id="books" class="form-context-fixed" style="display: none;">
        <jsp:include page="../form/contextAddBook.jsp"/>
      </section>

      <section id="bookInfo" class="form-context-fixed" style="display: none;">
        <jsp:include page="../form/contextInfoBook.jsp"/>
      </section>
    </section>

    <section>
      <section id="authors" class="form-context-fixed" style="display: none;">
        <jsp:include page="../form/contextAddAuthor.jsp"/>
      </section>

      <section id="authorInfo" class="form-context-fixed" style="display: none;">
        <jsp:include page="../form/contextInfoAuthor.jsp"/>
      </section>
    </section>

    <section>
      <section id="bookmarks" class="form-context-fixed" style="display: none;">
        <jsp:include page="../form/contextAddBookmark.jsp"/>
      </section>

      <section id="bookmarkInfo" class="form-context-fixed" style="display: none;">
        <jsp:include page="../form/contextInfoBookmark.jsp"/>
      </section>
    </section>

    <script charset="utf-8">
	let books = [${books}];
    let authors = [${authors}];
    let bookmarks = [${bookmarks}];
	</script>
    <script src="js/dependency.js" charset="utf-8"></script>
    <script src="js/book/book.js" charset="utf-8"></script>
    <script src="js/book/bookInfo.js" charset="utf-8"></script>
    <script src="js/authorDTO/authorDTO.js" charset="utf-8"></script>
    <script src="js/authorDTO/authorInfo.js" charset="utf-8"></script>
    <script src="js/bookmark/bookmark.js" charset="utf-8"></script>
    <script src="js/bookmark/bookmarkInfo.js" charset="utf-8"></script>
    <script src="js/listener.js" charset="utf-8"></script>
  </body>
</html>
