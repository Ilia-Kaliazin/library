<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="context-box box-font m">
    <header class="header-context-box">
        <p>Добавление автора</p>
        <button onclick="setDisplay('authors', 'none')">Выход</button>
    </header>
    <form class="fixed-box" action="home/additionAuthor" method="post">
        <table>
            <tr>
                <td><p>Имя</p></td>
                <td><input type="text" name="authorName" required="true" placeholder="Введите значения"></td>
            </tr>
            <tr>
                <td><p>Фамилия</p></td>
                <td><input type="text" name="authorLastName" required="true" placeholder="Введите значения"></td>
            </tr>
            <tr>
                <td><p>Отчество</p></td>
                <td><input type="text" name="authorSecondName" placeholder="Введите значения"></td>
            </tr>
            <tr>
                <td><p>Дата рождения</p></td>
                <td><input type="date" name="authorDob" required="true" placeholder="Введите значения"></td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" value="Добавить"></td>
            </tr>
        </table>
    </form>
</div>
