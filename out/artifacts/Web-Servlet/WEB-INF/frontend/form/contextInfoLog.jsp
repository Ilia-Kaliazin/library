<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="context-box box-font m">
    <header class="header-context-box">
        <p>Информация о логе</p>
        <button onclick="setDisplay('logInfo', 'none')">Выход</button>
    </header>
    <div class="fixed-box">
        <table id="logInfoTable"></table>
    </div>
</div>
