<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${isContextMsg}">
    <section>
        <div id="contextMsg" class="context-box box-font">
            <c:choose>
                <c:when test="${contextMsg == 'Login busy.'}"><p>Логин занят.</p></c:when>
                <c:when test="${contextMsg == 'User is blocked.'}"><p>Пользователь заблокирован.</p></c:when>
                <c:when test="${contextMsg == 'User not found.'}"><p>Неправильный логин или пароль.</p></c:when>
                <c:when test="${contextMsg == 'User not registration.'}"><p>Длинна логина или пароля превышает допустимый лимит.</p></c:when>
                <c:otherwise><p>${contextMsg}</p></c:otherwise>
            </c:choose>
        </div>
    </section>
    <c:remove var="isContextMsg"/>
    <c:remove var="contextMsg"/>
</c:if>