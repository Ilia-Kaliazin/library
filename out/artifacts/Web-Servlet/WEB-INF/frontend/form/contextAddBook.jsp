<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="context-box box-font m">
    <header class="header-context-box">
        <p>Добавление книги</p>
        <button onclick="setDisplay('books', 'none')">Выход</button>
    </header>
    <form class="fixed-box" action="home/additionBook" method="post">
        <table>
            <tr>
                <td><p>Название книги</p></td>
                <td><input type="text" name="nameBook" required="true" placeholder="Введите значения"></td>
            </tr>
            <tr>
                <td><p>Год выпуска</p></td>
                <td><input type="text" name="releaseYear" required="true" placeholder="Введите значения"></td>
            </tr>
            <tr>
                <td><p>Кол-во страниц</p></td>
                <td><input type="text" name="pageCount" required="true" placeholder="Введите значения"></td>
            </tr>
            <tr>
                <td><p>Опубликован</p></td>
                <td><input type="text" name="publisher" required="true" placeholder="Введите значения"></td>
            </tr>
            <tr>
                <td><p>ISBN</p></td>
                <td><input type="text" name="isbn" required="true" placeholder="Введите значения"></td>
            </tr>
            <tr>
                <td><p>Имя автора</p></td>
                <td><input type="text" name="authorName" required="true" placeholder="Введите значения"></td>
            </tr>
            <tr>
                <td><p>Фамилия</p></td>
                <td><input type="text" name="authorLastName" required="true" placeholder="Введите значения"></td>
            </tr>
            <tr>
                <td><p>Отчество</p></td>
                <td><input type="text" name="authorSecondName" placeholder="Введите значения"></td>
            </tr>
            <tr>
                <td><p>Дата рождения</p></td>
                <td><input type="date" name="authorDob" required="true" placeholder="Введите значения"></td>
            </tr>
            <tr>
                <td><input type="submit" value="Добавить"></td>
            </tr>
        </table>
    </form>
</div>
<div class="context-box box-font m" style="margin-top: 1em;">
    <form class="fixed-box" action="home/additionBook/upload" method="post" enctype="multipart/form-data">
        <table>
            <tr>
                <td colspan="2" style="text-align: center; font-weight: bold;">
                    <p>Добавление из файла</p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="file" name="file" required="true" accept=".json, .csv">
                </td>
            </tr>
            <tr>
                <td><input type="submit" value="Добавить"></td>
            </tr>
        </table>
    </form>
</div>