<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="context-box box-font m">
    <header class="header-context-box">
        <p>Инф. пользователя</p>
        <button onclick="setDisplay('userInfo', 'none')">Выход</button>
    </header>
    <div class="fixed-box">
        <table id="userInfoTable"></table>
    </div>
</div>
