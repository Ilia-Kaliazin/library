<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="context-box box-font m">
    <header class="header-context-box">
        <p>Информация о книге</p>
        <button onclick="setDisplay('bookInfo', 'none')">Выход</button>
    </header>
    <form class="fixed-box" action="home/additionBookmark" method="post">
        <table id="bookInfoTable"></table>
    </form>
</div>
