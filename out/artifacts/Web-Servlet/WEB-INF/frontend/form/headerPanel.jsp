<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<header id="header" class="box-font">
    <span>${user.role.name}</span>
    <p>${user.login}</p>
    <c:choose>
        <c:when test="${loginOn}">
            <form action="logoutOn" method="post">
                <input type="submit" value="Выйти из аккаунта">
            </form>
        </c:when>
        <c:otherwise>
            <form action="logout" method="post">
                <input type="submit" value="Выйти">
            </form>
        </c:otherwise>
    </c:choose>
    <form action="home" method="post">
        <input type="submit" value="Библиотека">
    </form>
</header>
