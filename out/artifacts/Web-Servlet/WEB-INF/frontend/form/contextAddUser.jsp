<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="context-box box-font m">
    <header class="header-context-box">
        <p>Добавление user</p>
        <button onclick="setDisplay('addUser', 'none')">Выход</button>
    </header>
    <form class="fixed-box" action="adminPanel/additionUser" method="post">
        <table>
            <tr>
                <td><p>Логин</p></td>
                <td><input type="text" name="login" required="true" placeholder="Введите значения"></td>
            </tr>
            <tr>
                <td><p>Пароль</p></td>
                <td><input type="text" name="password" required="true" placeholder="Введите значения"></td>
            </tr>
            <tr>
                <td><p>Подтвердите пароль</p></td>
                <td><input type="text" name="confirm-password" required="true" placeholder="Введите значения"></td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" value="Добавить"></td>
            </tr>
        </table>
    </form>
</div>
