<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="context-box box-font m">
    <header class="header-context-box">
        <p>Информация о авторе</p>
        <button onclick="setDisplay('authorInfo', 'none')">Выход</button>
    </header>
    <form class="fixed-box" action="home/deletionAuthor" method="post">
        <table id="authorInfoTable"></table>
    </form>
</div>
