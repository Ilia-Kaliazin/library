<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="context-box box-font m">
    <header class="header-context-box">
        <p>Информация о закладке</p>
        <button onclick="setDisplay('bookmarkInfo', 'none')">Выход</button>
    </header>
    <form class="fixed-box" action="home/deletionBookmark" method="post">
        <table id="bookmarkInfoTable"></table>
    </form>
</div>
