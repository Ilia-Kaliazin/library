<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="context-box box-font m">
    <header class="header-context-box">
        <p>Добавление закладки</p>
        <button onclick="setDisplay('bookmarks', 'none')">Выход</button>
    </header>
    <form class="fixed-box" action="home/additionBookmark" method="post">
        <table>
            <tr>
                <td><p>ISBN книги</p></td>
                <td><input type="text" name="isbn" required="true" placeholder="Введите значения"></td>
            </tr>
            <tr>
                <td><p>Страница</p></td>
                <td><input type="text" name="page" required="true" placeholder="Введите значения"></td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" value="Добавить"></td>
            </tr>
        </table>
    </form>
</div>