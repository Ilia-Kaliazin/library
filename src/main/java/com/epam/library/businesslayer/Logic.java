package com.epam.library.businesslayer;

import com.epam.library.businesslayer.analyzer.book.CSV;
import com.epam.library.businesslayer.analyzer.book.JSON;
import com.epam.library.businesslayer.procedures.adding.AuthorAdding;
import com.epam.library.businesslayer.procedures.adding.BookAdding;
import com.epam.library.businesslayer.procedures.adding.BookmarkAdding;
import com.epam.library.businesslayer.procedures.constructor.AuthorConstructor;
import com.epam.library.businesslayer.procedures.constructor.BookConstructor;
import com.epam.library.businesslayer.procedures.constructor.BookmarkConstructor;
import com.epam.library.businesslayer.procedures.constructor.UserConstructor;
import com.epam.library.businesslayer.procedures.deletion.AuthorDeletion;
import com.epam.library.businesslayer.procedures.deletion.BookDeletion;
import com.epam.library.businesslayer.procedures.deletion.BookmarkDeletion;
import com.epam.library.businesslayer.procedures.entrance.Login;
import com.epam.library.businesslayer.procedures.entrance.Registration;
import com.epam.library.businesslayer.procedures.list.BookmarksList;
import com.epam.library.businesslayer.procedures.list.BooksList;
import com.epam.library.console.*;
import com.epam.library.console.commands.CHelp;
import com.epam.library.exceptions.*;
import com.epam.library.model.Author;
import com.epam.library.model.Book;
import com.epam.library.model.Bookmark;
import com.epam.library.model.User;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;

public class Logic implements ICommand, ICommandAdmin, IEntrance {
    public static User user;
    // Dependency
    private final Console console;
    protected OutputFormat output;
    // Instance
    private final LogicAdmin logicAdmin;
    private static Logic instance;

    private Logic() {
        this.console = Console.getInstance(this);
        this.output = OutputFormat.getInstance(console);
        this.logicAdmin = LogicAdmin.getInstance(console, this);
        // Run
        console.start();
        console.setNameApp("JDBC");
        // Welcome
        CHelp.getInstance(console).welcome();
    }

    public static Logic getInstance() {
        if (instance == null) return instance = new Logic();
        return instance;
    }

    /**
     * Request
     */
    protected User requestInfoOfUser() {
        return UserConstructor.getInstance(console).run(new User());
    }

    protected Author requestInfoOfAuthor() {
        return AuthorConstructor.getInstance(console).run();
    }

    protected Book requestInfoOfBook() {
        return BookConstructor.getInstance(console).run();
    }

    protected Bookmark requestInfoOfMark() {
        return BookmarkConstructor.getInstance(console).run(user);
    }

    /**
     * Validation
     */
    private boolean isLogIn() {
        if (user == null) console.printFormat("Error", "Log in.");
        return user != null;
    }

    /**
     * Entrance
     */
    @Override
    public void onRegister() {
        if (user == null) {
            try {
                user = Registration.getInstance().run(requestInfoOfUser());
                console.printFormat("Registration", "User created.");
            } catch (UserIncorrectException | LoginBusyException | RegistrationException e) {
                console.printFormat("Error", e.getMessage());
            }
        } else console.printFormat("Warn", "You are already logged in.");
    }

    @Override
    public void onLogin(String[] arrString) {
        if (arrString.length == 3) {
            if (user == null) {
                try {
                    user = Login.getInstance().login(
                            new User(arrString[1], DigestUtils.md5Hex(arrString[2])));

                    console.printFormat("Library", "Success. You have entered the library.");
                    console.printFormat("Info", "Write <help> then you will see a list of commands.");
                } catch (UserIncorrectException | UserNotFoundException | UserBlockingException e) {
                    console.printFormat("Error", e.getMessage());
                }
            } else console.printFormat("Warn", "You are already logged in.");
        } else console.printFormat("Error", "Incorrect.");
    }

    @Override
    public void onLogout() {
        if (isLogIn()) {
            console.printFormat("Logout", "Success. You got the library out.");
            user = null;
        }
    }

    /**
     * Info
     */
    @Override
    public void onHelp() {
        if (null == user) {
            CHelp.getInstance(console).welcome();
            return;
        }
        CHelp.getInstance(console).print(user);
    }

    @Override
    public void onStatus() {
        if (isLogIn()) OutputFormat.getInstance(console).printInfoUser(user);
    }

    /**
     * Adding book or author
     */
    public void addBookFromFile(String path) {
        try {

            if (path.contains(".json"))
                BookAdding.getInstance(console).add(JSON.getInstance().run(path));
            else if (path.contains(".csv"))
                BookAdding.getInstance(console).add(CSV.getInstance().run(path));

        } catch (FileNotFoundException e) {
            console.printFormat("Error", "File not found.");
        } catch (IOException | BookIncorrectException | AuthorIncorrectException e) {
            console.printFormat("Error", e.getMessage());
        }
    }

    @Override
    public void onAdd(String[] arrString) {
        if (isLogIn()) {
            if (2 <= arrString.length) {
                switch (arrString[1]) {
                    case "book":
                        if (arrString.length == 3) addBookFromFile(arrString[2]);
                        else {
                            try {
                                BookAdding.getInstance(console).add(requestInfoOfBook());
                            } catch (BookIncorrectException | AuthorIncorrectException e) {
                                console.printFormat("Error", e.getMessage());
                            }
                        }
                        break;

                    case "author":
                        try {
                            AuthorAdding.getInstance(console).add(requestInfoOfAuthor());
                        } catch (AuthorIncorrectException e) {
                            console.printFormat("Error", e.getMessage());
                        }
                        break;

                    case "bookmark":
                        try {
                            BookmarkAdding.getInstance(console).add(requestInfoOfMark());
                        } catch (BookmarkExceedPagesException | BookmarkIncorrectException e) {
                            console.printFormat("Error", e.getMessage());
                        }
                        break;

                    case "user":
                        logicAdmin.onAdd();
                        break;
                }
            } else console.printFormat("Error", "Incorrect.");
        }
    }


    /**
     * Drop book or author
     */
    @Override
    public void onRemove(String[] arrString) {
        if (isLogIn()) {
            if (2 <= arrString.length) {
                switch (arrString[1]) {
                    case "book":
                        if (3 == arrString.length) {
                            try {
                                BookDeletion.getInstance(console).delete(arrString[2], null);
                            } catch (BookIncorrectException | BookNotFoundException e) {
                                console.printFormat("Error", e.getMessage());
                            }
                        }
                        break;

                    case "author":
                        try {
                            AuthorDeletion.getInstance(console).delete(requestInfoOfAuthor());
                        } catch (AuthorIncorrectException | AuthorNotFoundException e) {
                            console.printFormat("Error", e.getMessage());
                        }
                        break;

                    case "bookmark":
                        try {
                            BookmarkDeletion.getInstance(console).delete(arrString[2]);
                        } catch (BookmarkNotFoundException | BookmarkIncorrectException e) {
                            console.printFormat("Error", e.getMessage());
                        }
                        break;
                }
            } else console.printFormat("Error", "Incorrect.");
        }
    }

    /**
     * List.
     */
    @Override
    public void onList(String[] arrString) {
        if (isLogIn()) {
            if (2 <= arrString.length) {
                int indexF = -1;
                for (int i = 0; i < arrString.length; i++) if (arrString[i].contains("-f")) indexF = i;
                String[] subArr = Arrays.copyOfRange(arrString, indexF + 1, arrString.length);

                switch (arrString[1]) {
                    case "books":
                        if (indexF != -1) output.printBooksList(BooksList.getInstance().run(subArr));
                        else output.printBooksList(BooksList.getInstance().run(null));
                        break;

                    case "bookmarks":
                        if (indexF != -1) output.printBookmarksList(BookmarksList.getInstance().run(user, subArr));
                        else output.printBookmarksList(BookmarksList.getInstance().run(user, null));
                        break;

                    case "users":
                        if (indexF != -1) logicAdmin.onListUsers(subArr);
                        else logicAdmin.onListUsers(null);
                        break;

                    case "logs":
                        if (indexF != -1)
                            if (indexF == 3) logicAdmin.onListLogs(arrString[indexF - 1], subArr);
                            else logicAdmin.onListLogs(user.getLogin(), subArr);
                        else if (arrString.length == 3) logicAdmin.onListLogs(arrString[2], null);
                        else logicAdmin.onListLogs(user.getLogin(), null);
                        break;
                }
            } else console.printFormat("Error", "Incorrect.");
        }
    }

    /**
     * Update user line
     */
    @Override
    public void onUpdateRole(String[] strings) {
        if (isLogIn()) {
            if (4 == strings.length) {
                if ("role".equals(strings[1]))
                    logicAdmin.onUpdateUser(strings);
            } else console.printFormat("Error", "Incorrect.");
        }
    }

    /**
     * User blocked
     */
    @Override
    public void onBan(String[] strings) {
        if (isLogIn()) {
            if (2 == strings.length) {
                logicAdmin.onBannedUser(strings[1]);
            } else console.printFormat("Error", "Incorrect.");
        }
    }

    @Override
    public void onUnBan(String[] strings) {
        if (isLogIn()) {
            if (2 == strings.length) {
                logicAdmin.onUnBannedUser(strings[1]);
            } else console.printFormat("Error", "Incorrect.");
        }
    }

    /**
     * Выход.
     */
    @Override
    public void onExit() {
        console.printFormat("Exit", "Bye bye.");
        console.interrupt();
    }
}
