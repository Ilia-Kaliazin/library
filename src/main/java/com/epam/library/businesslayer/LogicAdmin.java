package com.epam.library.businesslayer;

import com.epam.library.businesslayer.procedures.blocking.UserLock;
import com.epam.library.businesslayer.procedures.entrance.Registration;
import com.epam.library.businesslayer.procedures.list.LogsList;
import com.epam.library.businesslayer.procedures.list.UsersList;
import com.epam.library.businesslayer.procedures.search.UserSearch;
import com.epam.library.businesslayer.procedures.updating.user.RoleUpdating;
import com.epam.library.console.Console;
import com.epam.library.dao.Logs;
import com.epam.library.exceptions.*;
import com.epam.library.model.Role;
import com.epam.library.model.User;

public class LogicAdmin {
    // Dependency
    private final Logic logic;
    private final Console console;
    private final UserSearch userSearch;
    // Instance
    private static LogicAdmin instance;

    private LogicAdmin(Console console, Logic logic) {
        this.logic = logic;
        this.console = console;
        this.userSearch = UserSearch.getInstance();
    }

    public static LogicAdmin getInstance(Console console, Logic logic) {
        if (instance == null) return instance = new LogicAdmin(console, logic);
        return instance;
    }

    public static boolean isRole(User user, Role Role) {
        return Role.name().equals(user.getRole().getName());
    }

    private boolean isPermission() {
        if (null != Logic.user) {
            if (isRole(Logic.user, Role.ADMIN)) return true;
            else console.printFormat("Error", "Permission denied!");
        }
        return false;
    }

    protected void onAdd() {
        if (isPermission()) {
            try {
                User user = Registration.getInstance().run(logic.requestInfoOfUser());
                Logs.getInstance().request(
                        Logic.user,
                        "Users",
                        user.getLogin() + ":" + user.getRole(),
                        "Create");
            } catch (UserIncorrectException | LoginBusyException | RegistrationException e) {
                console.printFormat("Error", e.getMessage());
            }
        }
    }

    protected void onUpdateUser(String[] strings) {
        if (isPermission()) {
            if (Logic.user.getLogin().equals(strings[2]))
                console.printFormat("Error", "You can not update role to myself.");
            else {
                try {
                    RoleUpdating.getInstance(console).update(strings[2], strings[3]);
                    console.printFormat("Good", "Update done.");
                } catch (UserIncorrectException | UserNotFoundException | UserUpdatingException | RoleIncorrectException | RoleNotFoundException e) {
                    console.printFormat("Error", e.getMessage());
                }
            }
        }
    }

    protected void onBannedUser(String login) {
        if (isPermission()) {
            if (Logic.user.getLogin().equals(login))
                console.printFormat("Error", "You can not banned myself.");
            else {
                try {
                    User user = userSearch.getUserByLogin(login);
                    if (user.isBanned()) console.printFormat("Error", "User banned.");
                    else {
                        UserLock.getInstance(Console.getInstance(null)).blockUser(user, true);
                        console.printFormat("Good", "Banned done.");
                    }
                } catch (UserNotFoundException e) {
                    console.printFormat("Error", "User not found.");
                } catch (UserIncorrectException | UserBlockingException e) {
                    console.printFormat("Error", e.getMessage());
                }
            }
        }
    }

    protected void onUnBannedUser(String login) {
        if (isPermission()) {
            try {
                User user = userSearch.getUserByLogin(login);
                if (!user.isBanned()) console.printFormat("Error", "User not banned.");
                else {
                    UserLock.getInstance(Console.getInstance(null)).blockUser(user, false);
                    console.printFormat("Good", "Unbanned done.");
                }
            } catch (UserNotFoundException e) {
                console.printFormat("Error", "User not found.");
            } catch (UserIncorrectException | UserBlockingException e) {
                console.printFormat("Error", e.getMessage());
            }
        }
    }

    protected void onListUsers(String[] strings) {
        if (isPermission())
            logic.output.printUsersList(UsersList.getInstance().run(strings));
    }

    protected void onListLogs(String login, String[] strings) {
        if (isPermission()) {
            try {
                logic.output.printLogsList(LogsList.getInstance().run(login, strings));
            } catch (UserIncorrectException e) {
                console.printFormat("Error", e.getMessage());
            }
        }
    }
}
