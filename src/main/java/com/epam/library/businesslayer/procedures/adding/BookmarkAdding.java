package com.epam.library.businesslayer.procedures.adding;

import com.epam.library.businesslayer.Logic;
import com.epam.library.businesslayer.procedures.search.BookSearch;
import com.epam.library.businesslayer.procedures.search.BookmarkSearch;
import com.epam.library.console.Console;
import com.epam.library.dao.Bookmarks;
import com.epam.library.dao.Logs;
import com.epam.library.exceptions.*;
import com.epam.library.model.Book;
import com.epam.library.model.Bookmark;

public class BookmarkAdding {
    private final Console console;
    private final BookmarkSearch bookmarkSearch;
    private static BookmarkAdding instance;

    private BookmarkAdding(Console console) {
        this.console = console;
        this.bookmarkSearch = BookmarkSearch.getInstance();
    }

    public static BookmarkAdding getInstance(Console console) {
        if (instance == null) return instance = new BookmarkAdding(console);
        return instance;
    }

    private void formPrint(String string) {
        console.printFormat("BookmarkAdding", string);
    }

    private boolean confirmBookCountPage(Bookmark templateBookmark) throws BookmarkExceedPagesException {
        try {
            Book book = BookSearch.getInstance().getBook(templateBookmark.getBook().getUUID());
            if (templateBookmark.getPage() <= book.getPageCount()) return true;
            else throw new BookmarkExceedPagesException("Bookmark should not exceed the number of pages.");
        } catch (BookIncorrectException | BookNotFoundException ignore) {
            return false;
        }
    }

    private void createBookmark(Bookmark templateBookmark) throws BookmarkExceedPagesException, BookmarkIncorrectException {
        if (!confirmBookCountPage(templateBookmark)) throw new BookmarkIncorrectException();
        formPrint("Create a new bookmark..");

        Bookmarks.getInstance().createBookmark(templateBookmark);
        try {
            Logs.getInstance().request(
                    Logic.user,
                    "Bookmarks",
                    BookmarkSearch.getInstance().getBookmark(templateBookmark).getUUID(),
                    "Create");
        } catch (BookmarkNotFoundException ignore) {
        }
    }

    private Bookmark confirmCreation(Bookmark templateBookmark) throws BookmarkIncorrectException {
        try {
            Bookmark bookmark = bookmarkSearch.getBookmark(templateBookmark);
            formPrint("Bookmark created.");
            return bookmark;
        } catch (BookmarkNotFoundException e) {
            throw new BookmarkIncorrectException("Fail create bookmark.");
        }
    }

    private Bookmark verifyBookmarkLock(Bookmark templateBookmark) throws BookmarkNotFoundException, BookmarkIncorrectException {
        Bookmark bookmark = bookmarkSearch.getBookmark(templateBookmark);
        if (!bookmark.isHidden()) formPrint("Bookmark will find.");
        else {
            Bookmarks.getInstance().updateMark(templateBookmark, "is_hidden", "0");
            Logs.getInstance().request(
                    Logic.user,
                    "Bookmarks",
                    bookmark.getUUID(),
                    "Update hidden:false");
            formPrint("Bookmark will find and unlocked.");
        }
        return bookmark;
    }

    public Bookmark add(Bookmark templateBookmark) throws BookmarkExceedPagesException, BookmarkIncorrectException {
        try {
            return verifyBookmarkLock(templateBookmark);
        } catch (BookmarkIncorrectException | BookmarkNotFoundException e) {
            createBookmark(templateBookmark);
            return confirmCreation(templateBookmark);
        }
    }
}
