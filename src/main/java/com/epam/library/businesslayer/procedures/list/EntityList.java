package com.epam.library.businesslayer.procedures.list;

import java.util.ArrayList;
import java.util.List;

public interface EntityList<T> {
    default List<T> getList(T entity){
        List<T> list = new ArrayList<>();
        list.add(entity);
        return list;
    }
}
