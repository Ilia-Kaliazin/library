package com.epam.library.businesslayer.procedures.constructor;

import com.epam.library.model.User;
import com.epam.library.console.Console;
import org.apache.commons.codec.digest.DigestUtils;

public class UserConstructor {
    private User user;
    private final Console console;
    private static UserConstructor instance;

    private UserConstructor(Console console) {
        this.console = console;
    }

    public static UserConstructor getInstance(Console console) {
        if (instance == null) return instance = new UserConstructor(console);
        return instance;
    }

    private String formRequest(String string) {
        return console.inputValue("Constructor", String.format("Give me %s user: ", string));
    }

    private String generateMD5(String string){
        return DigestUtils.md5Hex(string);
    }

    private void requestUserConfirmationPassword(){
        if(!user.getPassword().equals(generateMD5(formRequest("confirm password")))) user = null;
    }

    private void requestUserPassword(){
        user.setPassword(generateMD5(formRequest("password")));
        if(null == user.getLogin()) requestUserPassword();
        requestUserConfirmationPassword();
    }

    private void requestUserLogin(){
        user.setLogin(formRequest("login").replaceAll("\\s", ""));
        if(null == user.getLogin()) requestUserLogin();
        requestUserPassword();
    }

    public User run(User templateUser){
        user = templateUser;
        requestUserLogin();
        return user;
    }
}
