package com.epam.library.businesslayer.procedures.constructor;

import com.epam.library.businesslayer.procedures.search.BookSearch;
import com.epam.library.exceptions.BookIncorrectException;
import com.epam.library.exceptions.BookNotFoundException;
import com.epam.library.model.User;
import com.epam.library.model.Book;
import com.epam.library.model.Bookmark;
import com.epam.library.console.Console;

public class BookmarkConstructor {
    private Bookmark bookmark;
    private final Console console;
    private static BookmarkConstructor instance;

    private BookmarkConstructor(Console console) {
        this.console = console;
    }

    public static BookmarkConstructor getInstance(Console console) {
        if (instance == null) return instance = new BookmarkConstructor(console);
        return instance;
    }

    private String formRequest(String string) {
        return console.inputValue("Constructor", String.format("Give me %s: ", string));
    }

    private void requestPage() {
        try {
            bookmark.setPage(Integer.parseInt(formRequest("page bookmark")));
            if (0 >= bookmark.getPage()) requestPage();
        } catch (NumberFormatException ignore) {
            requestPage();
        }
    }

    private void requestBookUUID() {
        try {
            if (null == bookmark.getBook()) {
                bookmark.setBook(BookSearch.getInstance().getBook(formRequest("book uuid")));
                requestPage();
            }
        } catch (BookIncorrectException | BookNotFoundException ignore) {
            requestBookUUID();
        }
    }

    public Bookmark run(User user) {
        bookmark = new Bookmark();
        bookmark.setUser(user);

        requestBookUUID();
        return bookmark;
    }
}
