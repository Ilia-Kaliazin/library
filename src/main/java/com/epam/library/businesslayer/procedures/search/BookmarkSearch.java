package com.epam.library.businesslayer.procedures.search;

import com.epam.library.dao.Bookmarks;
import com.epam.library.exceptions.*;
import com.epam.library.model.Bookmark;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BookmarkSearch {
    private final Bookmarks bookmarks;
    private static BookmarkSearch instance;

    private BookmarkSearch() {
        this.bookmarks = Bookmarks.getInstance();
    }

    public static BookmarkSearch getInstance() {
        if (instance == null) return instance = new BookmarkSearch();
        return instance;
    }

    public Bookmark fillForm(ResultSet resultSql) throws SQLException {
        try {
            return new Bookmark(
                    resultSql.getString("uuid"),
                    resultSql.getInt("page"),
                    resultSql.getBoolean("is_hidden"),
                    UserSearch.getInstance().getUserByUUID(resultSql.getString("user_uuid")),
                    BookSearch.getInstance().getBook(resultSql.getString("book_uuid"))
            );
        } catch (UserNotFoundException |
                UserIncorrectException |
                BookIncorrectException |
                BookNotFoundException e) {
            return null;
        }
    }

    private Bookmark getBookmarkSqlByUUID(String uuid) throws BookmarkNotFoundException {
        try (ResultSet resultSql = bookmarks.getBookmarkByKeyAndValue("bm.uuid", uuid)) {
            if (resultSql != null && resultSql.last() && resultSql.getRow() == 1) {
                return fillForm(resultSql);
            }
        } catch (SQLException ignore) {
        }
        throw new BookmarkNotFoundException("Bookmark not found.");
    }

    private Bookmark getBookmarkSqlByInfo(Bookmark templateBookmark) throws BookmarkNotFoundException {
        try (ResultSet resultSql = bookmarks.getBookmarkByInfo(templateBookmark)) {
            if (resultSql != null && resultSql.last() && resultSql.getRow() == 1) {
                return fillForm(resultSql);
            }
        } catch (SQLException ignore) {
        }
        throw new BookmarkNotFoundException("Bookmark not found.");
    }

    public Bookmark getBookmark(String uuid) throws BookmarkIncorrectException, BookmarkNotFoundException {
        if (null == uuid) throw new BookmarkIncorrectException("Bookmark incorrect.");
        return getBookmarkSqlByUUID(uuid);
    }

    public Bookmark getBookmark(Bookmark templateBookmark) throws BookmarkIncorrectException, BookmarkNotFoundException {
        if (null == templateBookmark) throw new BookmarkIncorrectException("Bookmark incorrect.");
        return getBookmarkSqlByInfo(templateBookmark);
    }
}
