package com.epam.library.businesslayer.procedures.search.filters;

import com.epam.library.dao.Logs;

import java.sql.ResultSet;

public class FilterLogList extends Filter {
    private static FilterLogList instance;

    private FilterLogList() {
    }

    public static FilterLogList getInstance() {
        if (instance == null) return instance = new FilterLogList();
        return instance;
    }

    public String switchCase(String tag, String value) {
        switch (tag) {
            case "table":
                return " AND l.selected_table LIKE '" + value + "%'";
            case "field":
                return " AND l.selected_field LIKE '" + value + "%'";
            case "type":
                return " AND l.request_type LIKE '" + value + "%'";
            case "date":
                return " AND l.request_date = '" + value + "'";
        }
        return "";
    }

    public ResultSet searchWithFilter(String login, String[] strings) {
        StringBuilder stringBuffer = new StringBuilder();
        for (String string : strings) stringBuffer.append(splitParameters(string));
        return Logs.getInstance().getBooksByFilter(login, stringBuffer.toString());
    }
}
