package com.epam.library.businesslayer.procedures.search;

import com.epam.library.dao.Users;
import com.epam.library.exceptions.RoleIncorrectException;
import com.epam.library.exceptions.RoleNotFoundException;
import com.epam.library.exceptions.UserIncorrectException;
import com.epam.library.exceptions.UserNotFoundException;
import com.epam.library.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserSearch {
    private static UserSearch instance;

    private UserSearch() {
    }

    public static UserSearch getInstance() {
        if (instance == null) return instance = new UserSearch();
        return instance;
    }

    public User fillForm(ResultSet resultSql) throws SQLException {
        try {
            return new User(
                    resultSql.getString("uuid"),
                    RoleSearch.getInstance().getRole(resultSql.getString("role_uuid")),
                    resultSql.getString("login"),
                    resultSql.getString("password"),
                    resultSql.getBoolean("is_banned")
            );
        } catch (RoleIncorrectException | RoleNotFoundException e) {
            return null;
        }
    }

    private User getUserByParameter(String key, String value) throws UserNotFoundException {
        try (ResultSet resultSql = Users.getInstance().getUserByKeyAndValue(key, value)) {
            if (resultSql != null && resultSql.last() && resultSql.getRow() == 1) {
                return fillForm(resultSql);
            }
        } catch (SQLException ignore) {
        }
        throw new UserNotFoundException("User not found.");
    }

    private User getUserAccountByInputData(String login, String password) throws UserNotFoundException {
        try (ResultSet resultSql = Users.getInstance().getAccountUser(login, password)) {
            if (resultSql != null && resultSql.last() && resultSql.getRow() == 1) {
                return fillForm(resultSql);
            }
        } catch (SQLException ignore) {
        }
        throw new UserNotFoundException("User not found.");
    }

    public User getUserByLogin(String login) throws UserNotFoundException, UserIncorrectException {
        if (null == login) throw new UserIncorrectException("User incorrect.");
        else return getUserByParameter("login", login);
    }

    public User getUserByUUID(String uuid) throws UserNotFoundException, UserIncorrectException {
        if (null == uuid) throw new UserIncorrectException("User incorrect.");
        else return getUserByParameter("uuid", uuid);
    }

    public User getUserByData(String login, String password) throws UserNotFoundException, UserIncorrectException {
        if (null == login || null == password) throw new UserIncorrectException("User incorrect.");
        else return getUserAccountByInputData(login, password);
    }

    public User getUser(User templateUser) throws UserIncorrectException, UserNotFoundException {
        if (null == templateUser) throw new UserIncorrectException("User incorrect.");
        return getUserByData(templateUser.getLogin(), templateUser.getPassword());
    }
}
