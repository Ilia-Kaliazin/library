package com.epam.library.businesslayer.procedures.adding;

import com.epam.library.businesslayer.Logic;
import com.epam.library.businesslayer.procedures.search.AuthorSearch;
import com.epam.library.console.Console;
import com.epam.library.dao.Authors;
import com.epam.library.dao.Logs;
import com.epam.library.exceptions.AuthorIncorrectException;
import com.epam.library.exceptions.AuthorNotFoundException;
import com.epam.library.model.Author;

public class AuthorAdding {
    private final Console console;
    private final AuthorSearch authorSearch;
    private static AuthorAdding instance;

    private AuthorAdding(Console console) {
        this.console = console;
        this.authorSearch = AuthorSearch.getInstance();
    }

    public static AuthorAdding getInstance(Console console) {
        if (instance == null) return instance = new AuthorAdding(console);
        return instance;
    }

    private void formPrint(String string) {
        console.printFormat("AuthorAdding", string);
    }

    private void createAuthor(Author templateAuthor) {
        formPrint("Create a new author..");

        Authors.getInstance().createAuthor(templateAuthor);
        try {
            Logs.getInstance().request(
                    Logic.user,
                    "Authors",
                    AuthorSearch.getInstance().getAuthor(templateAuthor).getUUID(),
                    "Create");
        } catch (AuthorIncorrectException | AuthorNotFoundException ignore) {
        }
    }

    private Author confirmCreation(Author templateAuthor) throws AuthorIncorrectException {
        try {
            Author author = authorSearch.getAuthor(templateAuthor);
            formPrint("Author created.");
            return author;
        } catch (AuthorNotFoundException e) {
            throw new AuthorIncorrectException("Fail create author.");
        }
    }

    private Author verifyAuthorLock(Author templateAuthor) throws AuthorNotFoundException, AuthorIncorrectException {
        Author author = authorSearch.getAuthor(templateAuthor);
        if (!author.isHidden()) formPrint("Author will find.");
        else {
            Authors.getInstance().updateAuthor(author, "is_hidden", "0");
            Logs.getInstance().request(
                    Logic.user,
                    "Authors",
                    author.getUUID(),
                    "Update hidden:false");
            formPrint("Author will find and unlocked.");
        }
        return author;
    }

    public Author add(Author templateAuthor) throws AuthorIncorrectException {
        try {
            return verifyAuthorLock(templateAuthor);
        } catch (AuthorNotFoundException e) {
            createAuthor(templateAuthor);
            return confirmCreation(templateAuthor);
        }
    }
}
