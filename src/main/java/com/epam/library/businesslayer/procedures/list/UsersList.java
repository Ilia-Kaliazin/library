package com.epam.library.businesslayer.procedures.list;

import com.epam.library.businesslayer.procedures.search.UserSearch;
import com.epam.library.businesslayer.procedures.search.filters.FilterUserList;
import com.epam.library.dao.Users;
import com.epam.library.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UsersList {
    private static UsersList instance;

    private UsersList() {
    }

    public static UsersList getInstance() {
        if (instance == null) return instance = new UsersList();
        return instance;
    }

    private List<User> getUsersList(ResultSet resultSql) {
        if (null != resultSql) {
            List<User> userList = new ArrayList<>();

            try {
                while (resultSql.next()) {
                    userList.add(UserSearch.getInstance().fillForm(resultSql));
                }
                resultSql.close();
            } catch (SQLException ignore) {
            }
            return userList;
        }
        return null;
    }

    public List<User> run(String[] strings) {
        if (strings != null)
            return getUsersList(FilterUserList.getInstance().searchWithFilter(strings));
        return getUsersList(Users.getInstance().getList());
    }
}
