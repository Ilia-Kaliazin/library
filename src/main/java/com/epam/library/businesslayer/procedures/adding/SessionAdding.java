package com.epam.library.businesslayer.procedures.adding;

import com.epam.library.businesslayer.procedures.search.SessionSearch;
import com.epam.library.dao.Sessions;
import com.epam.library.exceptions.SessionIncorrectException;
import com.epam.library.exceptions.SessionNotFoundException;
import com.epam.library.exceptions.UserIncorrectException;
import com.epam.library.model.Session;
import com.epam.library.model.User;

import java.util.UUID;

public class SessionAdding {
    private final Sessions sessions;
    private final SessionSearch sessionSearch;
    private static SessionAdding instance;

    private SessionAdding() {
        this.sessions = Sessions.getInstance();
        this.sessionSearch = SessionSearch.getInstance();
    }

    public static SessionAdding getInstance() {
        if (instance == null) return instance = new SessionAdding();
        return instance;
    }

    private Session confirmCreation(Session session) throws SessionNotFoundException, UserIncorrectException {
        return sessionSearch.getSession(session.getUser());
    }

    private Session createSession(Session templateSession) throws UserIncorrectException, SessionNotFoundException {
        try {
            sessionSearch.getSession(templateSession.getUser());
            sessions.updateSession(templateSession);
        } catch (SessionNotFoundException e) {
            sessions.createSession(templateSession);
        }
        return confirmCreation(templateSession);
    }

    public Session add(User user, String token) throws UserIncorrectException, SessionNotFoundException {
        Session templateSession = new Session(null, user, token);
        return createSession(templateSession);
    }

    public Session add(User user) throws UserIncorrectException, SessionNotFoundException {
        Session templateSession = new Session(null, user, UUID.randomUUID().toString());
        return createSession(templateSession);
    }
}
