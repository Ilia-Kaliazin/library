package com.epam.library.businesslayer.procedures.deletion;

import com.epam.library.businesslayer.Logic;
import com.epam.library.businesslayer.procedures.search.BookmarkSearch;
import com.epam.library.console.Console;
import com.epam.library.dao.Bookmarks;
import com.epam.library.dao.Logs;
import com.epam.library.exceptions.BookmarkIncorrectException;
import com.epam.library.exceptions.BookmarkNotFoundException;
import com.epam.library.model.Bookmark;

public class BookmarkDeletion {
    private final Console console;
    private final BookmarkSearch bookmarkSearch;
    private static BookmarkDeletion instance;

    private BookmarkDeletion(Console console) {
        this.console = console;
        bookmarkSearch = BookmarkSearch.getInstance();
    }

    public static BookmarkDeletion getInstance(Console console) {
        if (instance == null) return instance = new BookmarkDeletion(console);
        return instance;
    }

    private void formPrint(String string) {
        console.printFormat("BookmarkDeletion", string);
    }

    private void lockBookmark(Bookmark bookmark) {
        formPrint("Lock bookmark..");

        Bookmarks.getInstance().updateMark(bookmark, "is_hidden", "1");
        Logs.getInstance().request(Logic.user,
                "Bookmarks",
                bookmark.getUUID(),
                "Update hidden:true");
    }

    private void confirmDeletion(Bookmark bookmark) throws BookmarkNotFoundException, BookmarkIncorrectException {
        if(bookmarkSearch.getBookmark(bookmark.getUUID()).isHidden()) formPrint("Bookmark is locked.");
        else throw new BookmarkIncorrectException("Bookmark is not locked.");
    }

    private void verifyBookmarkLock(Bookmark bookmark) throws BookmarkNotFoundException, BookmarkIncorrectException {
        if (bookmark.isHidden()) throw new BookmarkNotFoundException("Bookmark not found.");
        else {
            formPrint(String.format("Bookmark (UUID: %s) will find.", bookmark.getUUID().substring(0, 8)));
            lockBookmark(bookmark);
            confirmDeletion(bookmark);
        }
    }

    public void deleteBookmark(Bookmark bookmark) throws BookmarkIncorrectException, BookmarkNotFoundException {
        if(bookmark == null) throw new BookmarkIncorrectException("Bookmark incorrect.");
        else verifyBookmarkLock(bookmark);
    }

    public void delete(String uuid) throws BookmarkNotFoundException, BookmarkIncorrectException {
        deleteBookmark(bookmarkSearch.getBookmark(uuid));
    }
}
