package com.epam.library.businesslayer.procedures.search;

import com.epam.library.dao.Authors;
import com.epam.library.exceptions.AuthorIncorrectException;
import com.epam.library.exceptions.AuthorNotFoundException;
import com.epam.library.model.Author;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AuthorSearch {
    private static AuthorSearch instance;

    private AuthorSearch() {
    }

    public static AuthorSearch getInstance() {
        if (instance == null) return instance = new AuthorSearch();
        return instance;
    }

    private String verifySecondName(String string) {
        if (null == string || string.equals("null")) return "(none)";
        return string;
    }

    public Author fillForm(ResultSet resultSql) throws SQLException {
        return new Author(
                resultSql.getString("uuid"),
                resultSql.getString("name"),
                verifySecondName(resultSql.getString("secondName")),
                resultSql.getString("lastName"),
                resultSql.getDate("dob"),
                resultSql.getBoolean("is_hidden")
        );
    }

    private Author getAuthorSqlByUUID(String uuid) throws AuthorNotFoundException {
        try (ResultSet resultSql = Authors.getInstance().getAuthorAccountByUUID(uuid)) {
            if (resultSql != null && resultSql.last() && resultSql.getRow() == 1) {
                return fillForm(resultSql);
            }
        } catch (SQLException ignore) {
        }
        throw new AuthorNotFoundException("Author not found.");
    }

    private Author getAuthorSqlByInfo(Author templateAuthor) throws AuthorNotFoundException {
        try (ResultSet resultSql = Authors.getInstance().getAuthorAccountByInfo(templateAuthor)) {
            if (resultSql != null && resultSql.last() && resultSql.getRow() == 1) {
                return fillForm(resultSql);
            }
        } catch (SQLException ignore) {
        }
        throw new AuthorNotFoundException("Author not found.");
    }

    public Author getAuthor(String uuid) throws AuthorNotFoundException, AuthorIncorrectException {
        if (null == uuid) throw new AuthorIncorrectException("Author incorrect.");
        return getAuthorSqlByUUID(uuid);
    }

    public Author getAuthor(Author templateAuthor) throws AuthorIncorrectException, AuthorNotFoundException {
        if (null == templateAuthor) throw new AuthorIncorrectException("Author incorrect.");
        return getAuthorSqlByInfo(templateAuthor);
    }
}
