package com.epam.library.businesslayer.procedures.search;

import com.epam.library.dao.Logs;
import com.epam.library.exceptions.*;
import com.epam.library.model.Log;
import com.epam.library.model.Session;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LogSearch {
    private static LogSearch instance;

    private LogSearch() {
    }

    public static LogSearch getInstance() {
        if (instance == null) return instance = new LogSearch();
        return instance;
    }

    public Log fillForm(ResultSet resultSql) throws SQLException {
        try {
            return new Log(
                    resultSql.getString("uuid"),
                    UserSearch.getInstance().getUserByUUID(resultSql.getString("user_uuid")),
                    resultSql.getString("selected_table"),
                    resultSql.getString("selected_field"),
                    resultSql.getString("request_type"),
                    resultSql.getDate("request_date")
            );
        } catch (UserNotFoundException | UserIncorrectException e) {
            return null;
        }
    }

    private Log getLogSql(String uuid) throws LogNotFoundException {
        try (ResultSet resultSql = Logs.getInstance().getLogByUUID(uuid)) {
            if (resultSql != null && resultSql.last() && resultSql.getRow() == 1) {
                return fillForm(resultSql);
            }
        } catch (SQLException ignore) {
        }
        throw new LogNotFoundException("Log not found.");
    }

    public Log getLog(String uuid) throws LogIncorrectException, LogNotFoundException {
        if (null == uuid) throw new LogIncorrectException("Log incorrect.");
        else return getLogSql(uuid);
    }
}
