package com.epam.library.businesslayer.procedures.search.filters;

import com.epam.library.exceptions.ParseParameterException;

import static com.epam.library.console.Console.console;

public abstract class Filter {
    String splitParameters(String string) {
        try {
            String[] arr = splitParameter(string);
            return switchCase(arr[0], console().replaceTrash(arr[1]));
        } catch (ParseParameterException igonre) {
            return "";
        }
    }

    String[] splitParameter(String string) throws ParseParameterException {
        if (string.contains("=")) {
            String[] arr = string.split("=");
            arr[0] = arr[0].toLowerCase();
            if (arr.length == 2) return arr;
        }
        throw new ParseParameterException();
    }

    String switchCase(String tag, String value) {
        return null;
    }
}
