package com.epam.library.businesslayer.procedures.search.filters;

import com.epam.library.dao.Users;

import java.sql.ResultSet;

public class FilterUserList extends Filter {
    private static FilterUserList instance;

    private FilterUserList() {
    }

    public static FilterUserList getInstance() {
        if (instance == null) return instance = new FilterUserList();
        return instance;
    }

    private String verifyUserLock(String value) {
        if (Boolean.parseBoolean(value)) return " AND u.is_banned = 1";
        else return " AND u.is_banned = 0";
    }

    public String switchCase(String tag, String value){
        switch (tag) {
            case "userbanned":
                return verifyUserLock(value);
            case "userrole":
                return " AND r.name = '" + value + "'";
            case "username":
                return " AND u.login LIKE '" + value + "%'";
            case "usertag":
                return " AND u.uuid LIKE '" + value + "%'";
        }
        return "";
    }

    public ResultSet searchWithFilter(String[] strings) {
        StringBuilder stringBuffer = new StringBuilder();
        for (String string : strings) stringBuffer.append(splitParameters(string));
        return Users.getInstance().getBooksByFilter(stringBuffer.toString());
    }
}
