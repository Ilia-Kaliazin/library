package com.epam.library.businesslayer.procedures.search;

import com.epam.library.dao.Roles;
import com.epam.library.exceptions.RoleIncorrectException;
import com.epam.library.exceptions.RoleNotFoundException;
import com.epam.library.model.Role;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RoleSearch {
    private static RoleSearch instance;

    private RoleSearch() {
    }

    public static RoleSearch getInstance() {
        if (instance == null) return instance = new RoleSearch();
        return instance;
    }

    public Role fillForm(ResultSet resultSql) throws SQLException {
        Role role = Role.valueOf(resultSql.getString("name").toUpperCase());
        role.setUUID(resultSql.getString("uuid"));
        return role;
    }

    private Role getRoleByUUID(String uuid) throws RoleNotFoundException {
        try (ResultSet resultSql = Roles.getInstance().getRoleByUUID(uuid)) {
            if (resultSql != null && resultSql.last() && resultSql.getRow() == 1) {
                return fillForm(resultSql);
            }
        } catch (SQLException ignore) {
        }
        throw new RoleNotFoundException("Role not found.");
    }

    private Role getRoleByName(String name) throws RoleNotFoundException {
        try (ResultSet resultSql = Roles.getInstance().getRoleByName(name)) {
            if (resultSql != null && resultSql.last() && resultSql.getRow() == 1) {
                return fillForm(resultSql);
            }
        } catch (SQLException ignore) {
        }
        throw new RoleNotFoundException("Role not found.");
    }

    public Role getRole(String uuid) throws RoleIncorrectException, RoleNotFoundException {
        if (null == uuid) throw new RoleIncorrectException("Role incorrect.");
        else return getRoleByUUID(uuid);
    }

    public Role getRole(Role role) throws RoleIncorrectException, RoleNotFoundException {
        if (null == role) throw new RoleIncorrectException("Role incorrect.");
        else return getRoleByName(role.name());
    }
}
