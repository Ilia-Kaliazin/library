package com.epam.library.businesslayer.procedures.blocking;

import com.epam.library.businesslayer.Logic;
import com.epam.library.businesslayer.procedures.search.UserSearch;
import com.epam.library.console.Console;
import com.epam.library.dao.Logs;
import com.epam.library.dao.Users;
import com.epam.library.exceptions.UserBlockingException;
import com.epam.library.exceptions.UserIncorrectException;
import com.epam.library.exceptions.UserNotFoundException;
import com.epam.library.model.User;

public class UserLock {
    private final Console console;
    private static UserLock instance;

    private UserLock(Console console) {
        this.console = console;
    }

    public static UserLock getInstance(Console console) {
        if (instance == null) return instance = new UserLock(console);
        return instance;
    }

    private void updateUser(User user, boolean bool) {
        Users.getInstance().updateUserBanned(user, bool ? 1 : 0);
        Logs.getInstance().request(
                Logic.user,
                "Users",
                user.getUUID(),
                "Update banned:" + bool);
        console.printFormat(Logic.user, "UserBlocking", console.getUserContext(user) + ", " + bool);
    }

    private void verifyUserLock(User user, boolean bool) throws UserBlockingException {
        if(user.isBanned() != bool) updateUser(user, bool);
        else throw new UserBlockingException("User remained unchanged.");
    }

    public void blockUser(User user, boolean bool) throws UserIncorrectException, UserBlockingException {
        if(user == null) throw new UserIncorrectException("User incorrect.");
        else verifyUserLock(user, bool);
    }

    public void block(String uuid, boolean bool) throws UserIncorrectException, UserNotFoundException, UserBlockingException {
        if(uuid == null) throw new UserIncorrectException("User incorrect.");
        else blockUser(UserSearch.getInstance().getUserByUUID(uuid), bool);
    }
}
