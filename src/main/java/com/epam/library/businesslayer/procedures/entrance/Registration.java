package com.epam.library.businesslayer.procedures.entrance;

import com.epam.library.businesslayer.procedures.search.UserSearch;
import com.epam.library.dao.Users;
import com.epam.library.exceptions.LoginBusyException;
import com.epam.library.exceptions.RegistrationException;
import com.epam.library.exceptions.UserIncorrectException;
import com.epam.library.exceptions.UserNotFoundException;
import com.epam.library.model.User;

public class Registration {
    private final UserSearch userSearch;
    private static Registration instance;

    private Registration() {
        this.userSearch = UserSearch.getInstance();
    }

    public static Registration getInstance() {
        if (instance == null) return instance = new Registration();
        return instance;
    }

    private void createUser(User templateUser) {
        Users.getInstance().createUser(templateUser);
    }

    private User confirmCreation(User templateUser) throws RegistrationException {
        try {
            return userSearch.getUser(templateUser);
        } catch (UserNotFoundException | UserIncorrectException ignore) {
            throw new RegistrationException("User not registration.");
        }
    }

    private User verifyUserExistence(User templateUser) throws LoginBusyException, RegistrationException, UserIncorrectException {
        try {
            userSearch.getUserByLogin(templateUser.getLogin());
            throw new LoginBusyException("Login busy.");
        } catch (UserNotFoundException ignore) {
            createUser(templateUser);
            return confirmCreation(templateUser);
        }
    }

    public User run(User templateUser) throws UserIncorrectException, LoginBusyException, RegistrationException {
        if (null == templateUser) throw new UserIncorrectException("User incorrect.");
        else return verifyUserExistence(templateUser);
    }
}
