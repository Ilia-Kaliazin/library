package com.epam.library.businesslayer.procedures.list;

import com.epam.library.businesslayer.procedures.search.AuthorSearch;
import com.epam.library.businesslayer.procedures.search.filters.FilterAuthorList;
import com.epam.library.businesslayer.procedures.search.filters.FilterBookList;
import com.epam.library.dao.Authors;
import com.epam.library.model.Author;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AuthorsList {
    private static AuthorsList instance;

    private AuthorsList() {
    }

    public static AuthorsList getInstance() {
        if (instance == null) return instance = new AuthorsList();
        return instance;
    }

    private List<Author> getAuthorList(ResultSet resultSql) {
        if (null != resultSql) {
            Author author;
            List<Author> authorList = new ArrayList<>();

            try {
                while (resultSql.next()) {
                    author = AuthorSearch.getInstance().fillForm(resultSql);
                    if (!author.isHidden()) authorList.add(author);
                }
                resultSql.close();
            } catch (SQLException ignore) {
            }
            return authorList;
        }
        return null;
    }

    public List<Author> run(String[] filter) {
        if (filter != null)
            return getAuthorList(FilterAuthorList.getInstance().searchWithFilter(filter));
        return getAuthorList(Authors.getInstance().getList(0));
    }

}
