package com.epam.library.businesslayer.procedures.constructor;

import com.epam.library.console.Console;
import com.epam.library.model.Author;

import java.sql.Date;

public class AuthorConstructor {
    private final Console console;
    private static AuthorConstructor instance;

    private AuthorConstructor(Console console) {
        this.console = console;
    }

    public static AuthorConstructor getInstance(Console console) {
        if (instance == null) return instance = new AuthorConstructor(console);
        return instance;
    }

    private String formRequest(String string) {
        return console.inputValue("Constructor", String.format("Give me %s author: ", string));
    }

    private Author requestBirthDayAuthor(Author templateAuthor) {
        try {
            templateAuthor.setDob(Date.valueOf(formRequest("birthday")));
            return templateAuthor;
        } catch (IllegalArgumentException e) {
            return requestBirthDayAuthor(templateAuthor);
        }
    }

    private Author requestSecondNameAuthor(Author templateAuthor) {
        console.print("(Example) Write <none> then second name equals null.");
        templateAuthor.setSecondName(formRequest("second name"));

        if ("none".equals(templateAuthor.getSecondName())) {
            templateAuthor.setSecondName(null);
            return requestBirthDayAuthor(templateAuthor);
        }
        return requestBirthDayAuthor(templateAuthor);
    }

    private Author requestLastNameAuthor(Author templateAuthor) {
        templateAuthor.setLastName(formRequest("last name"));
        return requestSecondNameAuthor(templateAuthor);
    }

    private Author requestInfoAuthor(Author templateAuthor) {
        templateAuthor.setName(formRequest("name"));
        return requestLastNameAuthor(templateAuthor);
    }

    public Author run() {
        return requestInfoAuthor(new Author());
    }
}
