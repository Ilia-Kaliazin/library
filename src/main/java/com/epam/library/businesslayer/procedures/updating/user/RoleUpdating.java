package com.epam.library.businesslayer.procedures.updating.user;

import com.epam.library.businesslayer.Logic;
import com.epam.library.businesslayer.procedures.search.RoleSearch;
import com.epam.library.businesslayer.procedures.search.UserSearch;
import com.epam.library.console.Console;
import com.epam.library.dao.Logs;
import com.epam.library.dao.Users;
import com.epam.library.exceptions.*;
import com.epam.library.model.Role;
import com.epam.library.model.User;

public class RoleUpdating {
    private final Console console;
    private final UserSearch userSearch;
    private static RoleUpdating instance;

    private RoleUpdating(Console console) {
        this.console = console;
        this.userSearch = UserSearch.getInstance();
    }

    public static RoleUpdating getInstance(Console console) {
        if (instance == null) return instance = new RoleUpdating(console);
        return instance;
    }

    private void updateRole(User user, String role) throws RoleNotFoundException, RoleIncorrectException {
        Users.getInstance().updateUserRole(user, RoleSearch.getInstance().getRole(Role.valueOf(role)));
        Logs.getInstance().request(
                Logic.user,
                "Users",
                user.getUUID(),
                "Update role:" + role.toLowerCase());

        console.printFormat(Logic.user, "RoleUpdating", console.getUserContext(user) + " -> @" + role);
    }

    private void verifyUserRole(User user, String role) throws UserUpdatingException, RoleNotFoundException, RoleIncorrectException {
        if (!role.equals(user.getRole().getName())) updateRole(user, role);
        else throw new UserUpdatingException("User remained unchanged.");
    }

    public void updateUser(User user, String role) throws UserIncorrectException, UserUpdatingException, RoleNotFoundException, RoleIncorrectException {
        if (user == null || role == null) throw new UserIncorrectException("User incorrect.");
        verifyUserRole(user, role.toUpperCase());
    }

    public void update(String login, String role) throws UserIncorrectException, UserNotFoundException, UserUpdatingException, RoleNotFoundException, RoleIncorrectException {
        if (login == null || role == null) throw new UserIncorrectException("User incorrect.");
        else updateUser(userSearch.getUserByLogin(login), role);
    }
}
