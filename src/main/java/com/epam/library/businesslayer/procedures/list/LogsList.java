package com.epam.library.businesslayer.procedures.list;

import com.epam.library.businesslayer.Logic;
import com.epam.library.businesslayer.procedures.search.LogSearch;
import com.epam.library.businesslayer.procedures.search.filters.FilterLogList;
import com.epam.library.dao.Logs;
import com.epam.library.exceptions.UserIncorrectException;
import com.epam.library.model.Log;
import com.epam.library.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LogsList {
    private static LogsList instance;

    private LogsList() {
    }

    public static LogsList getInstance() {
        if (instance == null) return instance = new LogsList();
        return instance;
    }

    public List<Log> getUsersList(ResultSet resultSql) {
        if (null != resultSql) {
            List<Log> logList = new ArrayList<>();

            try {
                while (resultSql.next()) {
                    logList.add(LogSearch.getInstance().fillForm(resultSql));
                }
                resultSql.close();
            } catch (SQLException ignore) {
            }
            return logList;
        }
        return null;
    }

    public List<Log> run(String login, String[] strings) throws UserIncorrectException {
        if (login != null && strings == null) return getUsersList(Logs.getInstance().getLogsByUserLogin(login));
        else if (login != null) return getUsersList(FilterLogList.getInstance().searchWithFilter(login, strings));
        throw new UserIncorrectException("User incorrect.");
    }
}
