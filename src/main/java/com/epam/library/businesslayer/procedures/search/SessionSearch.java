package com.epam.library.businesslayer.procedures.search;

import com.epam.library.dao.Sessions;
import com.epam.library.exceptions.SessionIncorrectException;
import com.epam.library.exceptions.SessionNotFoundException;
import com.epam.library.exceptions.UserIncorrectException;
import com.epam.library.exceptions.UserNotFoundException;
import com.epam.library.model.Session;
import com.epam.library.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SessionSearch {
    private static SessionSearch instance;

    private SessionSearch() {
    }

    public static SessionSearch getInstance() {
        if (instance == null) return instance = new SessionSearch();
        return instance;
    }

    public Session fillForm(ResultSet resultSql) throws SQLException {
        try {
            return new Session(
                    resultSql.getString("uuid"),
                    UserSearch.getInstance().getUserByUUID(resultSql.getString("user_uuid")),
                    resultSql.getString("token")
            );
        } catch (UserNotFoundException | UserIncorrectException e) {
            return null;
        }
    }

    private Session getSessionRowByUser(User user) throws SessionNotFoundException {
        try (ResultSet resultSql = Sessions.getInstance().getSessionByUser(user)) {
            if (resultSql != null && resultSql.last() && resultSql.getRow() == 1) {
                return fillForm(resultSql);
            }
        } catch (SQLException ignore) {
        }
        throw new SessionNotFoundException("Session not found.");
    }

    private Session getSessionRowByToken(String token) throws SessionNotFoundException {
        try (ResultSet resultSql = Sessions.getInstance().getSessionByToken(token)) {
            if (resultSql != null && resultSql.last() && resultSql.getRow() == 1) {
                return fillForm(resultSql);
            }
        } catch (SQLException ignore) {
        }
        throw new SessionNotFoundException("Session not found.");
    }

    public Session getSession(User user) throws UserIncorrectException, SessionNotFoundException {
        if (null == user) throw new UserIncorrectException("User incorrect.");
        else return getSessionRowByUser(user);
    }

    public Session getSession(String token) throws SessionIncorrectException, SessionNotFoundException {
        if (null == token) throw new SessionIncorrectException("Token incorrect.");
        else return getSessionRowByToken(token);
    }
}
