package com.epam.library.businesslayer.procedures.list;

import com.epam.library.businesslayer.procedures.search.BookSearch;
import com.epam.library.businesslayer.procedures.search.filters.FilterBookList;
import com.epam.library.dao.Books;
import com.epam.library.model.Book;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BooksList {
    private static BooksList instance;

    private BooksList() {
    }

    public static BooksList getInstance() {
        if (instance == null) return instance = new BooksList();
        return instance;
    }

    private List<Book> getBooksList(ResultSet resultSql) {
        if (null != resultSql) {
            Book book;
            List<Book> bookList = new ArrayList<>();

            try {
                while (resultSql.next()) {
                    book = BookSearch.getInstance().fillForm(resultSql);
                    if (!book.getAuthor().isHidden()) bookList.add(book);
                }
                resultSql.close();
            } catch (SQLException ignore) {
            }
            return bookList;
        }
        return null;
    }

    public List<Book> run(String[] strings) {
        if (strings != null)
            return getBooksList(FilterBookList.getInstance().searchWithFilter(strings));
        return getBooksList(Books.getInstance().getList(0));
    }
}
