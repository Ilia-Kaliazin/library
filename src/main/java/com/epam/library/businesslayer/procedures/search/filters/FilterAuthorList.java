package com.epam.library.businesslayer.procedures.search.filters;

import com.epam.library.dao.Authors;

import java.sql.ResultSet;

public class FilterAuthorList extends Filter {
    private static FilterAuthorList instance;

    private FilterAuthorList() {
    }

    public static FilterAuthorList getInstance() {
        if (instance == null) return instance = new FilterAuthorList();
        return instance;
    }

    public String switchCase(String tag, String value) {
        switch (tag) {
            case "authoruuid":
                return " AND a.uuid LIKE '" + value + "%'";
            case "authorname":
                return " AND a.name LIKE '" + value + "%'";
            case "authorlastname":
                return " AND a.lastname LIKE '" + value + "%'";
            case "authorsecondname":
                return " AND a.secondname LIKE '" + value + "%'";
            case "authorDob":
                return " AND a.dob = '" + value + "%'";
        }
        return "";
    }

    public ResultSet searchWithFilter(String[] strings) {
        StringBuilder stringBuffer = new StringBuilder();
        for (String string : strings) stringBuffer.append(splitParameters(string));
        return Authors.getInstance().getAuthorsByFilter(stringBuffer.toString());
    }
}
