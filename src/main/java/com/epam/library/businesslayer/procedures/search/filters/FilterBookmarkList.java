package com.epam.library.businesslayer.procedures.search.filters;

import com.epam.library.dao.Bookmarks;
import com.epam.library.model.User;

import java.sql.ResultSet;

public class FilterBookmarkList extends Filter {
    private static FilterBookmarkList instance;

    private FilterBookmarkList() {
    }

    public static FilterBookmarkList getInstance() {
        if (instance == null) return instance = new FilterBookmarkList();
        return instance;
    }

    public String switchCase(String tag, String value) {
        switch (tag) {
            case "bookmarkuuid":
                return " AND bm.uuid LIKE '" + value + "%'";
            case "bookmarkpage":
                return FilterBookList.getInstance().verifyInputValueMoreOrLess("bm.page", value);
        }
        return FilterBookList.getInstance().switchCase(tag, value);
    }

    public ResultSet searchWithFilter(User user, String[] strings) {
        StringBuilder stringBuffer = new StringBuilder();
        for (String string : strings) stringBuffer.append(splitParameters(string));
        return Bookmarks.getInstance().getBookmarksByFilter(user, stringBuffer.toString());
    }
}
