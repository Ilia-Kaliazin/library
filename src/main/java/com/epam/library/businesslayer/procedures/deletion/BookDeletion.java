package com.epam.library.businesslayer.procedures.deletion;

import com.epam.library.businesslayer.Logic;
import com.epam.library.businesslayer.procedures.search.BookSearch;
import com.epam.library.console.Console;
import com.epam.library.dao.Books;
import com.epam.library.dao.Logs;
import com.epam.library.exceptions.BookIncorrectException;
import com.epam.library.exceptions.BookNotFoundException;
import com.epam.library.model.Book;

public class BookDeletion {
    private final Console console;
    private final BookSearch bookSearch;
    private static BookDeletion instance;

    private BookDeletion(Console console) {
        this.console = console;
        this.bookSearch = BookSearch.getInstance();
    }

    public static BookDeletion getInstance(Console console) {
        if (instance == null) return instance = new BookDeletion(console);
        return instance;
    }

    private void formPrint(String string) {
        console.printFormat("BookDeletion", string);
    }

    private void lockBook(Book book) {
        formPrint("Lock book..");

        Books.getInstance().updateBook(book, "is_hidden", "1");
        Logs.getInstance().request(Logic.user,
                "Books",
                book.getUUID(),
                "Update hidden:true");
    }

    private void confirmDeletion(Book book) throws BookIncorrectException, BookNotFoundException {
        if (bookSearch.getBook(book.getUUID()).isHidden()) formPrint("Book is locked.");
        else throw new BookIncorrectException("Book is not locked.");
    }

    private void verifyBookLock(Book book) throws BookNotFoundException, BookIncorrectException {
        if (book.isHidden()) throw new BookNotFoundException("Book not found.");
        else {
            formPrint(String.format("Book (UUID: %s) will find.", book.getUUID().substring(0, 8)));
            lockBook(book);
            confirmDeletion(book);
        }
    }

    public void deleteBook(Book book) throws BookIncorrectException, BookNotFoundException {
        if(book == null) throw new BookIncorrectException("Book incorrect.");
        else verifyBookLock(book);
    }

    public void delete(Book templateBook) throws BookIncorrectException, BookNotFoundException {
        if(templateBook == null) throw new BookIncorrectException("Book incorrect.");
        else deleteBook(bookSearch.getBook(templateBook));
    }

    public void delete(String isbn, String uuid) throws BookIncorrectException, BookNotFoundException {
        if(uuid == null && isbn != null) deleteBook(bookSearch.getBookByISBN(isbn));
        else if(uuid != null) deleteBook(bookSearch.getBook(uuid));
        else throw new BookIncorrectException("Value incorrect.");
    }
}
