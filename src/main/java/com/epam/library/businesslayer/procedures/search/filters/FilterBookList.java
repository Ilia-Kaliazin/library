package com.epam.library.businesslayer.procedures.search.filters;

import com.epam.library.dao.Books;

import java.sql.ResultSet;

public class FilterBookList extends Filter {
    private static FilterBookList instance;

    private FilterBookList() {
    }

    public static FilterBookList getInstance() {
        if (instance == null) return instance = new FilterBookList();
        return instance;
    }

    private int toInt(String string) throws NumberFormatException{
        return Integer.parseInt(string);
    }

    private String verifyReleaseYearInputValueIn(String value) {
        String[] tempString = value.split("-");

        try {
            int beginYear = Integer.parseInt(tempString[0]);
            int endYear = Integer.parseInt(tempString[1]);

            if (endYear > beginYear) {
                return " AND b.release_year BETWEEN '" + beginYear + "' AND '" + endYear + "'";
            }
        } catch (NumberFormatException ignore) {
        }
        return "";
    }

    public String verifyInputValueMoreOrLess(String tag, String value) {
        try {
            if ('<' == value.charAt(0)) {
                if (':' == value.charAt(1)) return " AND " + tag + " <= '" + toInt(value.substring(2)) + "'";
                return " AND " + tag + " < '" + toInt(value.substring(1)) + "'";
            }
            if ('>' == value.charAt(0)) {
                if (':' == value.charAt(1)) return " AND " + tag + " >= '" + toInt(value.substring(2)) + "'";
                return " AND " + tag + " > '" + toInt(value.substring(1)) + "'";
            }
            return " AND " + tag + " = '" + toInt(value.replaceAll(":", "")) + "'";
        } catch (NumberFormatException ignore) {
            return "";
        }
    }

    public String switchCase(String tag, String value){
        switch (tag) {
            case "bookuuid":
                return " AND b.uuid LIKE '" + value + "%'";
            case "bookname":
                return " AND b.name LIKE '" + value + "%'";
            case "bookisbn":
                return " AND b.isbn LIKE '" + value + "%'";
            case "bookreleaseyear":
                return verifyInputValueMoreOrLess("b.release_year", value);
            case "bookreleaseyearin":
                return verifyReleaseYearInputValueIn(value);
            case "bookpagecount":
                return verifyInputValueMoreOrLess("b.page_count", value);
            case "bookpublisher":
                return " AND b.publisher LIKE '" + value + "%'";
        }
        return FilterAuthorList.getInstance().switchCase(tag, value);
    }

    public ResultSet searchWithFilter(String[] strings) {
        StringBuilder stringBuffer = new StringBuilder();
        for (String string : strings) stringBuffer.append(splitParameters(string));
        return Books.getInstance().getBooksByFilter(stringBuffer.toString());
    }
}
