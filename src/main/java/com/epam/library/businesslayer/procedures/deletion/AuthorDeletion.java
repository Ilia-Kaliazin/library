package com.epam.library.businesslayer.procedures.deletion;

import com.epam.library.businesslayer.Logic;
import com.epam.library.businesslayer.procedures.list.BooksList;
import com.epam.library.businesslayer.procedures.search.AuthorSearch;
import com.epam.library.console.Console;
import com.epam.library.dao.Authors;
import com.epam.library.dao.Logs;
import com.epam.library.exceptions.AuthorIncorrectException;
import com.epam.library.exceptions.AuthorNotFoundException;
import com.epam.library.exceptions.BookIncorrectException;
import com.epam.library.exceptions.BookNotFoundException;
import com.epam.library.model.Author;
import com.epam.library.model.Book;

import java.util.List;

public class AuthorDeletion {
    private final Logs logs;
    private final Console console;
    private final AuthorSearch authorSearch;
    private static AuthorDeletion instance;

    private AuthorDeletion(Console console) {
        this.console = console;
        this.logs = Logs.getInstance();
        this.authorSearch = AuthorSearch.getInstance();
    }

    public static AuthorDeletion getInstance(Console console) {
        if (instance == null) return instance = new AuthorDeletion(console);
        return instance;
    }

    private void formPrint(String string) {
        console.printFormat("AuthorDeletion", string);
    }

    private void lockAuthor(Author author) {
        formPrint("Lock author..");

        Authors.getInstance().updateAuthor(author, "is_hidden", "1");
        logs.request(
                Logic.user,
                "Authors",
                author.getUUID(),
                "Update hidden:true");
    }

    private void lockAuthorBooks(Author author) {
        formPrint("Author books lock..");

        String[] filter = {"authoruuid=" + author.getUUID()};
        List<Book> list = BooksList.getInstance().run(filter);
        for(Book book : list) {
            try {
                BookDeletion.getInstance(console).deleteBook(book);
            } catch (BookIncorrectException | BookNotFoundException e) {
                console.printError(this, e);
            }
        }
    }

    private void confirmDeletion(Author author) throws AuthorIncorrectException, AuthorNotFoundException {
        if (authorSearch.getAuthor(author.getUUID()).isHidden()) formPrint("Author is locked.");
        else throw new AuthorIncorrectException("Author is not locked.");
    }

    private void verifyAuthorLock(Author author) throws AuthorNotFoundException, AuthorIncorrectException {
        if (author.isHidden()) throw new AuthorNotFoundException("Author not found.");
        else {
            formPrint(String.format("Author (UUID: %s) will find.", author.getUUID().substring(0, 8)));
            lockAuthorBooks(author);
            lockAuthor(author);
            confirmDeletion(author);
        }
    }

    public void deleteAuthor(Author author) throws AuthorIncorrectException, AuthorNotFoundException {
        if (author == null) throw new AuthorIncorrectException("Author incorrect.");
        verifyAuthorLock(author);
    }

    public void delete(String uuid) throws AuthorNotFoundException, AuthorIncorrectException {
        deleteAuthor(authorSearch.getAuthor(uuid));
    }

    public void delete(Author templateAuthor) throws AuthorIncorrectException, AuthorNotFoundException {
        if (templateAuthor == null) throw new AuthorIncorrectException("Author incorrect.");
        deleteAuthor(authorSearch.getAuthor(templateAuthor));
    }
}
