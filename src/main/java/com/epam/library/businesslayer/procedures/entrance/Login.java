package com.epam.library.businesslayer.procedures.entrance;

import com.epam.library.businesslayer.procedures.search.UserSearch;
import com.epam.library.exceptions.UserBlockingException;
import com.epam.library.exceptions.UserIncorrectException;
import com.epam.library.exceptions.UserNotFoundException;
import com.epam.library.model.User;

public class Login {
    private final UserSearch userSearch;
    private static Login instance;

    private Login() {
        this.userSearch = UserSearch.getInstance();
    }

    public static Login getInstance() {
        if (instance == null) return instance = new Login();
        return instance;
    }

    private User verifyUserLock(User user) throws UserBlockingException {
        if (user.isBanned()) throw new UserBlockingException("User is blocked.");
        else return user;
    }

    private User getUser(User templateUser) throws UserNotFoundException, UserIncorrectException, UserBlockingException {
        return verifyUserLock(userSearch.getUser(templateUser));
    }

    public User login(User templateUser) throws UserIncorrectException, UserNotFoundException, UserBlockingException {
        if (null == templateUser) throw new UserIncorrectException("Incorrect user.");
        else return getUser(templateUser);
    }
}
