package com.epam.library.businesslayer.procedures.adding;

import com.epam.library.businesslayer.Logic;
import com.epam.library.businesslayer.procedures.search.BookSearch;
import com.epam.library.console.Console;
import com.epam.library.dao.Books;
import com.epam.library.dao.Logs;
import com.epam.library.exceptions.AuthorIncorrectException;
import com.epam.library.exceptions.BookIncorrectException;
import com.epam.library.exceptions.BookNotFoundException;
import com.epam.library.model.Book;

import java.util.List;

public class BookAdding {
    private final Logs logs;
    private final Books books;
    private final Console console;
    private final BookSearch bookSearch;
    private static BookAdding instance;

    private BookAdding(Console console) {
        this.console = console;
        this.logs = Logs.getInstance();
        this.books = Books.getInstance();
        this.bookSearch = BookSearch.getInstance();
    }

    public static BookAdding getInstance(Console console) {
        if (instance == null) return instance = new BookAdding(console);
        return instance;
    }

    private void formPrint(String string) {
        console.printFormat("BookAdding", string);
    }

    private void createBookInTable(Book templateBook) throws BookIncorrectException {
        if (!templateBook.getISBN().contains("-")) throw new BookIncorrectException("ISBN incorrect!");
        formPrint("Create a new book..");

        books.createBook(templateBook);
        try {
            logs.request(
                    Logic.user,
                    "Books",
                    BookSearch.getInstance().getBook(templateBook).getUUID(),
                    "Create");
        } catch (BookNotFoundException ignore) {
        }
    }


    private Book confirmCreation(Book templateBook) throws BookIncorrectException {
        try {
            Book book = bookSearch.getBook(templateBook);
            formPrint("Book created.");
            return book;
        } catch (BookNotFoundException e) {
            throw new BookIncorrectException("Fail create book.");
        }
    }

    private Book verifyBookLock(Book templateBook) throws BookIncorrectException, BookNotFoundException {
        Book book = bookSearch.getBook(templateBook);
        if (!book.isHidden()) formPrint("Book will find.");
        else {
            books.updateBook(templateBook, "is_hidden", "0");
            logs.request(
                    Logic.user,
                    "Book",
                    book.getUUID(),
                    "Update hidden:false");
            formPrint("Book will find and unlocked.");
        }
        return book;
    }

    public Book add(Book templateBook) throws BookIncorrectException, AuthorIncorrectException {
        try {
            templateBook.setAuthor(AuthorAdding.getInstance(console).add(templateBook.getAuthor()));
            return verifyBookLock(templateBook);
        } catch (BookNotFoundException e) {
            createBookInTable(templateBook);
            return confirmCreation(templateBook);
        }
    }

    public void add(List<Book> list) throws BookIncorrectException, AuthorIncorrectException {
        if (list != null) {
            for (Book book : list) {
                add(book);
            }
        } else formPrint("List books empty.");
    }
}
