package com.epam.library.businesslayer.procedures.search;

import com.epam.library.dao.Books;
import com.epam.library.exceptions.AuthorIncorrectException;
import com.epam.library.exceptions.AuthorNotFoundException;
import com.epam.library.exceptions.BookIncorrectException;
import com.epam.library.exceptions.BookNotFoundException;
import com.epam.library.model.Book;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BookSearch {
    private static BookSearch instance;

    private BookSearch() {
    }

    public static BookSearch getInstance() {
        if (instance == null) return instance = new BookSearch();
        return instance;
    }

    /**
     * Constructor
     */
    public Book fillForm(ResultSet resultSql) throws SQLException {
        try {
            return new Book(
                    resultSql.getString("uuid"),
                    resultSql.getString("name"),
                    resultSql.getInt("release_year"),
                    resultSql.getInt("page_count"),
                    resultSql.getString("isbn"),
                    resultSql.getString("publisher"),
                    AuthorSearch.getInstance().getAuthor(resultSql.getString("author_uuid")),
                    resultSql.getBoolean("is_hidden"));
        } catch (AuthorNotFoundException | AuthorIncorrectException e) {
            return null;
        }
    }

    /**
     * ResultSet
     */
    private Book getBookSqlByUUID(String uuid) throws BookNotFoundException {
        try (ResultSet resultSql = Books.getInstance().getBookByUUID(uuid)) {
            if (resultSql != null && resultSql.last() && resultSql.getRow() == 1) {
                return fillForm(resultSql);
            }
        } catch (SQLException ignore) {
        }
        throw new BookNotFoundException("Book not found.");
    }

    private Book getBookSqlByInfo(Book book) throws BookNotFoundException {
        try (ResultSet resultSql = Books.getInstance().getBookByInfo(book)) {
            if (resultSql != null && resultSql.last() && resultSql.getRow() == 1) {
                return fillForm(resultSql);
            }
        } catch (SQLException ignore) {
        }
        throw new BookNotFoundException("Book not found.");
    }

    private Book getBookSqlByISBN(String isbn) throws BookNotFoundException {
        try (ResultSet resultSql = Books.getInstance().getBookByISBN(isbn)) {
            if (resultSql != null && resultSql.last() && resultSql.getRow() == 1) {
                return fillForm(resultSql);
            }
        } catch (SQLException ignore) {
        }
        throw new BookNotFoundException("Book not found.");
    }

    public Book getBook(Book templateBook) throws BookIncorrectException, BookNotFoundException {
        if (null == templateBook) throw new BookIncorrectException("Book incorrect.");
        return getBookSqlByInfo(templateBook);
    }

    public Book getBook(String uuid) throws BookIncorrectException, BookNotFoundException {
        if (null == uuid) throw new BookIncorrectException("Book incorrect.");
        return getBookSqlByUUID(uuid);
    }

    public Book getBookByISBN(String isbn) throws BookIncorrectException, BookNotFoundException {
        if (null == isbn) throw new BookIncorrectException("Book incorrect.");
        return getBookSqlByISBN(isbn);
    }
}
