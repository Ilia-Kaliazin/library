package com.epam.library.businesslayer.procedures.constructor;

import com.epam.library.model.Book;
import com.epam.library.console.Console;

public class BookConstructor {
    private final Console console;
    private static BookConstructor instance;

    private BookConstructor(Console console) {
        this.console = console;
    }

    public static BookConstructor getInstance(Console console) {
        if (instance == null) return instance = new BookConstructor(console);
        return instance;
    }

    private String formRequest(String string) {
        return console.inputValue("Constructor", String.format("Give me %s book: ", string));
    }

    private Book requestInfoOfAuthor(Book templateBook) {
        templateBook.setAuthor(AuthorConstructor.getInstance(console).run());
        return templateBook;
    }

    private Book requestInfoPublisher(Book templateBook) {
        templateBook.setPublisher(formRequest("publisher"));
        return requestInfoOfAuthor(templateBook);
    }

    private Book requestInfoISBN(Book templateBook) {
        templateBook.setISBN(formRequest("ISBN"));
        if (templateBook.getISBN().indexOf("-") == 3) return requestInfoPublisher(templateBook);
        else return requestInfoISBN(templateBook);
    }

    private Book requestInfoCountPage(Book templateBook) {
        try {
            templateBook.setPageCount(Integer.parseInt(formRequest("count")));
            if (templateBook.getPageCount() > 0) return requestInfoISBN(templateBook);
            else return requestInfoCountPage(templateBook);
        } catch (NumberFormatException e) {
            return requestInfoCountPage(templateBook);
        }
    }

    private Book requestInfoYear(Book templateBook) {
        try {
            templateBook.setReleaseYear(Integer.parseInt(formRequest("year")));
            if (1000 < templateBook.getReleaseYear() && templateBook.getReleaseYear() < 2021)
                return requestInfoCountPage(templateBook);
            else return requestInfoYear(templateBook);
        } catch (NumberFormatException e) {
            return requestInfoCountPage(templateBook);
        }
    }

    private Book requestInfoBook(Book templateBook) {
        templateBook.setBookName(formRequest("name"));
        return requestInfoYear(templateBook);
    }

    public Book run() {
        return requestInfoBook(new Book());
    }
}
