package com.epam.library.businesslayer.procedures.list;

import com.epam.library.businesslayer.procedures.search.BookmarkSearch;
import com.epam.library.businesslayer.procedures.search.filters.FilterBookmarkList;
import com.epam.library.dao.Bookmarks;
import com.epam.library.model.Bookmark;
import com.epam.library.model.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookmarksList {
    private static BookmarksList instance;

    private BookmarksList() {
    }

    public static BookmarksList getInstance() {
        if (instance == null) return instance = new BookmarksList();
        return instance;
    }

    private List<Bookmark> getBookmarksList(ResultSet resultSql) {
        if (null != resultSql) {
            Bookmark tempBookmark;
            List<Bookmark> bookmarksList = new ArrayList<>();

            try {
                while (resultSql.next()) {
                    tempBookmark = BookmarkSearch.getInstance().fillForm(resultSql);
                    if (!tempBookmark.getBook().isHidden()) bookmarksList.add(tempBookmark);
                }
                resultSql.close();
            } catch (SQLException ignore) {
            }
            return bookmarksList;
        }
        return null;
    }

    public List<Bookmark> run(User user, String[] strings) {
        if (strings != null)
            return getBookmarksList(FilterBookmarkList.getInstance().searchWithFilter(user, strings));
        return getBookmarksList(Bookmarks.getInstance().getBookmarkUserByKeyAndValue(user, "bm.is_hidden", "0"));
    }
}
