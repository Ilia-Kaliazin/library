package com.epam.library.businesslayer.analyzer.book;

import com.epam.library.model.Author;
import com.epam.library.model.Book;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class CSV {
    private static CSV instance;

    private CSV() {
    }

    public static CSV getInstance() {
        if (instance == null) return instance = new CSV();
        return instance;
    }

    private boolean isTitle(CSVRecord record){
        if(!"Book name".equals(record.get(0))) return false;
        if(!"Year".equals(record.get(1))) return false;
        if(!"Page count".equals(record.get(2))) return false;
        if(!"ISBN".equals(record.get(3))) return false;
        if(!"Publisher".equals(record.get(4))) return false;
        if(!"Last name".equals(record.get(5))) return false;
        if(!"First name".equals(record.get(6))) return false;
        if(!"Second name".equals(record.get(7))) return false;
        return "Birthday".equals(record.get(8));
    }


    private Date parsingDate(String string) throws java.text.ParseException {
        return Date.valueOf(
                new SimpleDateFormat("yyyy-mm-dd").format(
                        new SimpleDateFormat("dd.mm.yyyy").parse(
                                string)));
    }

    private List<Book> getList(Iterable<CSVRecord> recordIterable) {
        try {
            List<Book> bookList = new ArrayList<>();
            for (CSVRecord record : recordIterable)
                if (!isTitle(record) && record.size() == 9)
                    bookList.add(new Book(
                            null,
                            record.get(0),
                            Integer.parseInt(record.get(1)),
                            Integer.parseInt(record.get(2)),
                            record.get(3),
                            record.get(4),
                            new Author(
                                    null,
                                    record.get(6),
                                    record.get(7),
                                    record.get(5),
                                    parsingDate(record.get(8)),
                                    false
                            ),
                            false
                    ));
            return bookList;
        } catch (java.text.ParseException e) {
            return null;
        }
    }

    public List<Book> run(String path) throws IOException {
        return getList(CSVFormat.DEFAULT.parse(new FileReader(path)));
    }

    public List<Book> run(InputStreamReader inputStreamReader) throws IOException {
        return getList(CSVFormat.DEFAULT.parse(inputStreamReader));
    }
}
