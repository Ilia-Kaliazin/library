package com.epam.library.businesslayer.analyzer.book;

import com.epam.library.model.Author;
import com.epam.library.model.Book;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class JSON {
    private static JSON instance;

    private JSON() {
    }

    public static JSON getInstance() {
        if (instance == null) return instance = new JSON();
        return instance;
    }

    private Date parsingDate(String string) throws java.text.ParseException {
        return Date.valueOf(
                new SimpleDateFormat("yyyy-mm-dd").format(
                        new SimpleDateFormat("dd.mm.yyyy").parse(
                                string)));
    }

    public List<Book> getList(JSONArray jsonArray) {
        try {
            List<Book> bookList = new ArrayList<>();
            JSONParser jsonParser = new JSONParser();
            Iterator iterator = jsonArray.iterator();
            JSONObject jsonObject;

            while (iterator.hasNext()) {
                jsonObject = (JSONObject) iterator.next();
                JSONObject author = (JSONObject) jsonParser.parse(jsonObject.get("author").toString());
                bookList.add(new Book(
                        null,
                        jsonObject.get("bookName").toString(),
                        Integer.parseInt(jsonObject.get("releaseYear").toString()),
                        Integer.parseInt(jsonObject.get("pageCount").toString()),
                        jsonObject.get("ISBN").toString(),
                        jsonObject.get("publisher").toString(),
                        new Author(
                                null,
                                author.get("name").toString(),
                                author.get("secondName").toString(),
                                author.get("lastName").toString(),
                                parsingDate(author.get("dob").toString()),
                                false
                        ),
                        false
                ));
            }
            return bookList;
        } catch (ParseException | NumberFormatException | java.text.ParseException ignore) {
            return null;
        }
    }

    public List<Book> run(String path) throws IOException {
        try {
            return getList((JSONArray) new JSONParser().parse(new FileReader(path)));
        } catch (ParseException e) {
            return null;
        }
    }

    public List<Book> run(InputStreamReader inputStreamReader) throws IOException {
        try {
            return getList((JSONArray) new JSONParser().parse(inputStreamReader));
        } catch (ParseException e) {
            return null;
        }
    }
}
