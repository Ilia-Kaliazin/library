package com.epam.library.console;

public interface ICommand {
    void onAdd(String[] string);
    void onRemove(String[] string);
    void onList(String[] tempString);
    void onHelp();
    void onStatus();
    void onExit();
}
