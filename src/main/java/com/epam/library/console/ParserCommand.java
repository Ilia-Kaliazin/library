package com.epam.library.console;

public class ParserCommand {
    private final Console console;
    private final ICommand iCommand;
    private final ICommandAdmin iCommandAdmin;
    private final IEntrance iEntrance;
    private static ParserCommand instance;

    private ParserCommand(Object object, Console console) {
        this.console = console;
        this.iCommand = (ICommand) object;
        this.iCommandAdmin = (ICommandAdmin) object;
        this.iEntrance = (IEntrance) object;
    }

    public static ParserCommand getInstance(Object object, Console console) {
        if (instance == null) return instance = new ParserCommand(object, console);
        return instance;
    }

    public void run(String string) {
        String[] tempString = string.split("\\s");
        switch (tempString[0].toLowerCase()) {
            case "reg":
            case "register":
                iEntrance.onRegister();
                break;
            case "login":
                iEntrance.onLogin(tempString);
                break;
            case "logout":
                iEntrance.onLogout();
                break;
            case "add":
                iCommand.onAdd(tempString);
                break;
            case "rm":
            case "remove":
                iCommand.onRemove(tempString);
                break;
            case "ls":
            case "list":
                iCommand.onList(tempString);
                break;
            case "help":
                iCommand.onHelp();
                break;
            case "update":
                iCommandAdmin.onUpdateRole(tempString);
                break;
            case "ban":
                iCommandAdmin.onBan(tempString);
                break;
            case "unban":
                iCommandAdmin.onUnBan(tempString);
                break;
            case "status":
                iCommand.onStatus();
                break;
            case "exit":
                iCommand.onExit();
                break;
            default:
                console.printFormat("Error", "Command incorrect.");
        }
    }
}
