package com.epam.library.console;

import com.epam.library.businesslayer.Logic;
import com.epam.library.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Console extends Thread {
    private String nameApp = "App";
    private final Scanner scanner;
    private final ParserCommand parserCommand;
    private static Console instance;
    private static final Logger log = LogManager.getLogger(Console.class);

    private Console(ICommand iCommand) {
        this.parserCommand = ParserCommand.getInstance(iCommand, this);
        // New
        scanner = new Scanner(System.in);
    }

    public static Console getInstance(ICommand iCommand) {
        if (instance == null) return instance = new Console(iCommand);
        return instance;
    }

    public static Console console() {
        return getInstance(null);
    }

    public void print(String value) {
        log.info(value);
    }

    public void print(String tag, String value) {
        log.info(String.format("[%s] %s", tag, value));
    }

    // [App] Message.
    public void printFormat(String value) {
        log.info(String.format("[%s] %s.", nameApp, value));
    }

    // [App] (UserAdding) User incorrect.
    public void printFormat(String tag, String value) {
        log.info(String.format("[%s] {%s} %s -> %s.", nameApp, tag, value, getUserContext(Logic.user)));
    }

    //  [App] (BooksList) Book incorrect. -> User@admin
    public void printFormat(User user, String tag, String value) {
        log.info(String.format("[%s] {%s} (%s) %s.", nameApp, tag, getUserContext(user), value));
    }

    public void printError(Object obj, Throwable e) {
        String nameObj = obj.getClass().getName();
        String nameClass = nameObj.substring(nameObj.lastIndexOf(".") + 1);
        printFormat(nameClass, e.getMessage());
    }

    public String replaceTrash(String s){
        return s.replaceAll("[^a-zA-Z0-9\\-\\s\\.\\/\\:\\=\\<\\>]", "");
    }

    public String inputValue(String value) {
        return (value == null) ?  replaceTrash(scanner.nextLine()) : value;
    }

    public String inputValue(String tag, String request) {
        print(tag, request);
        return inputValue(null);
    }

    @Override
    public void run() {
        while (!isInterrupted()) parserCommand.run(inputValue(null));
        scanner.close();
    }

    public String getUserContext(User user){
        if(user == null) return "anonymous";
        return String.format("%s@%s", user.getLogin(), user.getRole().getName());
    }

    public void setNameApp(String nameApp) {
        this.nameApp = nameApp;
    }
}
