package com.epam.library.console;

public interface IEntrance {
    void onRegister();
    void onLogin(String[] string);
    void onLogout();
}
