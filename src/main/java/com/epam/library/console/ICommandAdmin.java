package com.epam.library.console;

public interface ICommandAdmin {
    void onUpdateRole(String[] tempString);
    void onBan(String[] tempString);
    void onUnBan(String[] tempString);
}
