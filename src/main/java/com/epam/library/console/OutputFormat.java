package com.epam.library.console;

import com.epam.library.model.Book;
import com.epam.library.model.Bookmark;
import com.epam.library.model.Log;
import com.epam.library.model.User;

import java.util.List;

public class OutputFormat {
    private final Console console;
    private static OutputFormat instance;

    private OutputFormat(Console console) {
        this.console = console;
    }

    public static OutputFormat getInstance(Console console) {
        if (instance == null) return instance = new OutputFormat(console);
        return instance;
    }

    private String getTopLine(int i) {
        return "‾";//.repeat(Math.max(0, i));
    }

    private String getLowLine(int i) {
        return "_";//.repeat(Math.max(0, i));
    }

    private String getSpace(int i) {
        return " ";//.repeat(Math.max(0, i));
    }

    private String getTitle(String title, int leftSpace, int rightSpace) {
        return getSpace(leftSpace) + title + getSpace(rightSpace);
    }

    private String getTitle(String title, int Space) {
        return getSpace(Space) + title + getSpace(Space);
    }

    private String hideCorrect(String string, int maxLength) {
        return string.length() > maxLength ? string.substring(0, maxLength - 2) + ".." : string;
    }

    public void printBooksList(List<Book> bookList) {
        if (null != bookList && !bookList.isEmpty()) {
            console.print(String.format("%s\n[%s][%s|%s|%s|%s|%s|%s|%s|%s|%s]",
                    getLowLine(150),
                    getTitle("Book library", 1),
                    getTitle("Book name", 6),
                    getTitle("ISBN", 6),
                    getTitle("YEAR", 1),
                    getTitle("PAGE", 1),
                    getTitle("PUBLISHER", 2, 1),
                    getTitle("Author name", 3),
                    getTitle("Second name", 3),
                    getTitle("Last name", 4),
                    getTitle("Birthday", 2)
            ));

            for (Book book : bookList) {
                console.print(String.format("[Book: %s] %20s | %14s | %4d | %4d | %10s | %15s | %15s | %15s | %10s |",
                        book.getUUID().substring(0, 8),
                        hideCorrect(book.getBookName(), 20),
                        hideCorrect(book.getISBN(), 14),
                        book.getReleaseYear(),
                        book.getPageCount(),
                        hideCorrect(book.getPublisher(), 10),
                        hideCorrect(book.getAuthor().getName(), 15),
                        hideCorrect(book.getAuthor().getSecondName(), 15),
                        hideCorrect(book.getAuthor().getLastName(), 15),
                        book.getAuthor().getDob()));
            }

            console.print(getTopLine(150));
        } else console.printFormat("Books", "Clear.");
    }

    public void printBookmarksList(List<Bookmark> bookmarkList) {
        if (null != bookmarkList && !bookmarkList.isEmpty()) {
            console.print(String.format("%s\n[%s][%s|%s|%s]",
                    getLowLine(57),
                    getTitle("Bookmarks ID", 3),
                    getTitle("Name book", 6),
                    getTitle("YEAR", 1),
                    getTitle("PAGE", 1)
            ));

            for (Bookmark bookMark : bookmarkList) {
                console.print(String.format("[Bookmark: %8s] %20s | %4d | %4d |",
                        bookMark.getUUID().substring(0, 8),
                        hideCorrect(bookMark.getBook().getBookName(), 20),
                        bookMark.getBook().getReleaseYear(),
                        bookMark.getPage()));
            }

            console.print(getTopLine(57));
        } else console.printFormat("Bookmarks", "Clear.");
    }

    public void printLogsList(List<Log> logList) {
        if (null != logList && !logList.isEmpty()) {
            console.print(String.format("%s\n[%s]%s|%s|%s|%s|%s]",
                    getLowLine(141),
                    getTitle("Logs", 5, 4),
                    getTitle("User: " + logList.get(0).getUser().getUUID().substring(0, 8), 4),
                    getTitle("TABLE", 4, 3),
                    getTitle("FIELD", 24),
                    getTitle("REQUEST TYPE", 5),
                    getTitle("DATE", 4)
            ));

            for (Log log : logList) {
                console.print(String.format("[Log: %8s] %20s | %10s | %51s | %20s | %10s |",
                        log.getUUID().substring(0, 8),
                        hideCorrect(log.getUser().getLogin(), 20),
                        log.getTable(),
                        hideCorrect(log.getField(), 51),
                        hideCorrect(log.getType(), 20),
                        log.getDate()));
            }

            console.print(getTopLine(141));
        } else console.printFormat("Logs", "Clear.");
    }

    public void printUsersList(List<User> userList) {
        if (null != userList && !userList.isEmpty()) {
            console.print(String.format("%s\n[%s][%s|%s|%s]",
                    getLowLine(56),
                    getTitle("Users", 5, 4),
                    getTitle("User name", 6),
                    getTitle("ROLES", 1),
                    getTitle("BANNED", 1)
            ));

            for (User user : userList) {
                console.print(String.format("[User: %8s] %20s | %5s |  %5b |",
                        user.getUUID().substring(0, 8),
                        hideCorrect(user.getLogin(), 20),
                        user.getRole().getName(),
                        user.isBanned()));
            }

            console.print(getTopLine(56));
        } else console.printFormat("Bookmarks", "Clear.");
    }

    public void printInfoUser(User user) {
        console.print(String.format("[UUID: %s] User info.\n" +
                        "| You login: %s\n" +
                        "| You roles: %s\n" +
                        "[Done]",
                user.getUUID().substring(0, 8),
                user.getLogin(),
                user.getRole().getName()));
    }
}
