package com.epam.library.console.commands;

import com.epam.library.console.Console;
import com.epam.library.model.User;

public class CHelp {
    private final Console console;
    private static CHelp instance;

    private CHelp(Console console) {
        this.console = console;
    }

    public static CHelp getInstance(Console console) {
        if (instance == null) return instance = new CHelp(console);
        return instance;
    }

    public void welcome() {
        console.printFormat("<<<[ Welcome on book library ]>>>\n" +
                "A wide selection of books awaits you!\n\n" +
                "[Sign In] Write: login <nickname> <password>, enter.\n" +
                "[Sign Up] Write: register, enter.");
    }

    private String listCommandAdmin() {
        return "\n[User] --- Work on User --- [User]\n" +
                "[1] add user       - Create a new user in the library.\n" +
                "[2] ban <login>    - Block library user.\n" +
                "[3] unban <login>  - Unblock library user.\n" +
                "[4] update role <login> <role> - Update role user.\n" +
                "[5] list users     - List library user.\n" +
                "[6] list logs <login> - Logs user." +
                "[7] list logs -f [date:<date>]  - bookmark search by date. (Type: DD-MM-YYYY)\n" +
                "                 [Table:<name>] - bookmark search by table.\n";

    }

    private String listCommandUser() {
        return "[Help] User commands:\n" +
                "[Author] --- Work on author --- [Author]\n" +
                "[1] add author    - Add in library new Author.\n" +
                "[2] remove author - Delete with Author library.\n\n" +
                "[Book] --- Work on book --- [Book]\n" +
                "[1] add book             - Add in library new Book.\n" +
                "[2] add book <path>      - Add in library new Book from file. (CSV or JSON)\n" +
                "[3] use <id>             - Use Book.\n" +
                "[4] remove book (ISBN)   - Delete with Book library.\n" +
                "[5] list books           - Book list.\n" +
                "[6] list books -f [bookName:<name>]    - book search by name.\n" +
                "                  [authorName:<name>]  - book search by Author name.\n" +
                "                  [isbn:<identificat>] - book search by identificat ISBN.\n" +
                "                  [publisher:<name>]   - book search by publisher.\n" +
                "                  [year:<year>]        - book search by year\n" +
                "                  [yearIn:<before year>-<next year>] - book search between years.\n" +
                "                  [pageCount:<count>]  - book search by page count.\n\n" +
                "[Bookmark] --- Work on bookmark --- [Bookmark]\n" +
                "[1] add bookmark    - Add bookmark in book.\n" +
                "[2] remove bookmark - Delete bookmark from book.\n" +
                "[3] list bookmarks  - Bookmark list.\n" +
                "[5] list bookmarks -f [name:<name>]  - bookmark search by name.\n" +
                "                      [year:<year>]  - bookmark search by year\n" +
                "                      [page:<count>] - bookmark search by page.";
    }

    public void print(User user) {
        if ("admin".equals(user.getRole())) console.printFormat(listCommandUser() + listCommandAdmin());
        else console.printFormat(listCommandUser());
    }
}
