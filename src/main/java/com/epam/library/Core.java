package com.epam.library;

import com.epam.library.businesslayer.Logic;
import com.epam.library.sql.Compound;

public class Core {
    public void run() {
        // createConnect
        Compound.getInstance().uploadDataSQL(
                "jdbc:oracle:thin:@localhost:1521:xe",
                "LIBRARY",
                "1");
        // Run
        Logic.getInstance();
    }
}

/*Config config = Config.getInstance();
config.setPathProperties("config.properties");
compound.uploadDataSQL(config);
config.close();*/