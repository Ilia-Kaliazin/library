package com.epam.library.dao;

import java.sql.ResultSet;

public class Roles extends DAO {
    private static Roles instance;

    private Roles() {
    }

    public static Roles getInstance() {
        if (instance == null) return instance = new Roles();
        return instance;
    }

    public ResultSet getRoleByName(String name) {
        createConnect();
        query.createPreparedQuery("SELECT * FROM Roles WHERE name = ?");
        query.setString(1, name.toLowerCase());
        return query.send();
    }

    public ResultSet getRoleByUUID(String uuid) {
        createConnect();
        query.createPreparedQuery("SELECT * FROM Roles WHERE uuid = ?");
        query.setString(1, uuid);
        return query.send();
    }
}
