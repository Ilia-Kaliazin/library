package com.epam.library.dao;

import com.epam.library.sql.Compound;
import com.epam.library.sql.query.Query;

public abstract class DAO {
    public Query query;
    private final Compound compound = Compound.getInstance();

    public void createConnect() {
        this.compound.connect();
        this.query = this.compound.query;
    }
}
