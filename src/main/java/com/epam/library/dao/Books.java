package com.epam.library.dao;

import com.epam.library.model.Book;

import java.sql.ResultSet;
import java.util.UUID;

public class Books extends DAO {
    private static Books instance;

    private Books() {
    }

    public static Books getInstance() {
        if (instance == null) return instance = new Books();
        return instance;
    }


    public ResultSet getList(int pointHidden) {
        createConnect();
        query.createPreparedQuery("SELECT * " +
                "FROM Books " +
                "WHERE is_hidden = ?");
        query.setInt(1, pointHidden);
        return query.send();
    }

    public ResultSet getBookByUUID(String uuid) {
        createConnect();
        query.createPreparedQuery("SELECT * " +
                "FROM Books " +
                "WHERE uuid LIKE ?");
        query.setString(1, uuid + "%");
        return query.send();
    }

    public ResultSet getBookByISBN(String isbn) {
        createConnect();
        query.createPreparedQuery("SELECT * " +
                "FROM Books " +
                "WHERE isbn = ?");
        query.setString(1, isbn);
        return query.send();
    }

    public ResultSet getBookByInfo(Book book) {
        createConnect();
        query.createPreparedQuery("SELECT * " +
                "FROM Books " +
                "WHERE author_uuid = ? " +
                "AND isbn = ? " +
                "AND name = ? " +
                "AND release_year = ? " +
                "AND page_count = ? " +
                "AND publisher = ?");
        query.setString(1, book.getAuthor().getUUID());
        query.setString(2, book.getISBN());
        query.setString(3, book.getBookName());
        query.setInt(4, book.getReleaseYear());
        query.setInt(5, book.getPageCount());
        query.setString(6, book.getPublisher());
        return query.send();
    }

    public ResultSet getBooksByFilter(String preparedQuery) {
        createConnect();
        query.createPreparedQuery("SELECT * " +
                "FROM Books b JOIN Authors a on b.author_uuid = a.uuid " +
                "WHERE b.is_hidden = '0'" + preparedQuery);
        return query.send();
    }

    public void createBook(Book book) {
        createConnect();
        query.createPreparedQuery("INSERT INTO Books " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, '0')");
        query.setString(1, UUID.randomUUID().toString());
        query.setString(2, book.getAuthor().getUUID());
        query.setString(3, book.getISBN());
        query.setString(4, book.getBookName());
        query.setInt(5, book.getReleaseYear());
        query.setInt(6, book.getPageCount());
        query.setString(7, book.getPublisher());
        query.execute();
    }

    public void updateBook(Book book, String tag, String newValue) {
        createConnect();
        query.createPreparedQuery("UPDATE Books " +
                "SET " + tag + " = ? " +
                "WHERE isbn = ? OR uuid = ?");
        query.setString(1, newValue);
        query.setString(2, book.getISBN());
        query.setString(3, book.getUUID());
        query.execute();
    }
}
