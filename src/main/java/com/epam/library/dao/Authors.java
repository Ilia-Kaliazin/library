package com.epam.library.dao;

import com.epam.library.model.Author;
import com.epam.library.sql.Compound;
import com.epam.library.sql.query.Query;

import java.sql.ResultSet;
import java.util.UUID;

public class Authors extends DAO {
    private static Authors instance;

    private Authors() {
    }

    public static Authors getInstance() {
        if (instance == null) return instance = new Authors();
        return instance;
    }

    public ResultSet getList(int pointHidden) {
        createConnect();
        query.createPreparedQuery("SELECT * " +
                "FROM Authors " +
                "WHERE is_hidden = ?");
        query.setInt(1, pointHidden);
        return query.send();
    }

    public ResultSet getAuthorAccountByInfo(Author author) {
        createConnect();
        query.createPreparedQuery("SELECT * " +
                "FROM Authors " +
                "WHERE name = ? " +
                "AND lastName = ? " +
                "AND dob = ? ");
        query.setString(1, author.getName());
        query.setString(2, author.getLastName());
        query.setDate(3, author.getDob());
        return query.send();
    }

    public ResultSet getAuthorAccountByUUID(String uuid) {
        createConnect();
        query.createPreparedQuery("SELECT * " +
                "FROM Authors " +
                "WHERE uuid = ? ");
        query.setString(1, uuid);
        return query.send();
    }

    public ResultSet getAuthorsByFilter(String preparedQuery) {
        createConnect();
        query.createPreparedQuery("SELECT * " +
                "FROM Authors a " +
                "WHERE a.is_hidden = '0'" + preparedQuery);
        return query.send();
    }

    public void createAuthor(Author author) {
        createConnect();
        query.createPreparedQuery("INSERT INTO Authors " +
                "VALUES (?, ?, ?, ?, ?, '0')");
        query.setString(1, UUID.randomUUID().toString());
        query.setString(2, author.getName());
        query.setString(3, author.getSecondName());
        query.setString(4, author.getLastName());
        query.setDate(5, author.getDob());
        query.execute();
    }

    public void updateAuthor(Author author, String tag, String newValue) {
        createConnect();
        query.createPreparedQuery("UPDATE Authors " +
                "SET " + tag + " = ? " +
                "WHERE uuid = ? ");
        query.setString(1, newValue);
        query.setString(2, author.getUUID());
        query.execute();
    }
}
