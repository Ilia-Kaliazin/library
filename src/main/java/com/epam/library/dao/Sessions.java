package com.epam.library.dao;

import com.epam.library.model.Session;
import com.epam.library.model.User;
import com.epam.library.sql.Compound;
import com.epam.library.sql.query.Query;
import org.apache.commons.codec.digest.DigestUtils;

import java.sql.ResultSet;
import java.util.UUID;

public class Sessions extends DAO {
    private static Sessions instance;

    private Sessions() {
    }

    public static Sessions getInstance() {
        if(instance == null) return instance = new Sessions();
        return instance;
    }

    public ResultSet getSessionByUser(User user) {
        createConnect();
        query.createPreparedQuery("SELECT * FROM Sessions WHERE user_uuid = ?");
        query.setString(1, user.getUUID());
        return query.send();
    }

    public ResultSet getSessionByToken(String token) {
        createConnect();
        query.createPreparedQuery("SELECT * FROM Sessions WHERE token = ?");
        query.setString(1, token);
        return query.send();
    }

    public void createSession(Session session) {
        createConnect();
        query.createPreparedQuery("INSERT INTO Sessions " +
                "VALUES (?, ?, ?)");
        query.setString(1, UUID.randomUUID().toString());
        query.setString(2, session.getUser().getUUID());
        query.setString(3, session.getToken());
        query.execute();
    }

    public void updateSession(Session session){
        createConnect();
        query.createPreparedQuery("UPDATE (SELECT * FROM Sessions " +
                "WHERE user_uuid = ?) " +
                "SET token = ?");
        query.setString(1, session.getUser().getUUID());
        query.setString(2, session.getToken());
        query.update();
    }
}
