package com.epam.library.dao;

import com.epam.library.model.Role;
import com.epam.library.model.User;
import com.epam.library.sql.Compound;
import com.epam.library.sql.query.Query;

import java.sql.ResultSet;
import java.util.UUID;

public class Users extends DAO {
    private static Users instance;

    private Users() {
    }

    public static Users getInstance() {
        if (instance == null) return instance = new Users();
        return instance;
    }

    public ResultSet getList() {
        createConnect();
        query.createPreparedQuery("SELECT * FROM Users");
        return query.send();
    }

    public ResultSet getBooksByFilter(String preparedQuery) {
        createConnect();
        query.createPreparedQuery("SELECT * " +
                "FROM Users u JOIN Roles r on u.role_uuid = r.uuid " +
                "WHERE is_banned > '-1'" +
                preparedQuery);
        return query.send();
    }

    public ResultSet getAccountUser(String login, String password) {
        createConnect();
        query.createPreparedQuery("SELECT * " +
                "FROM Users " +
                "WHERE login = ? " +
                "AND password = ?");
        query.setString(1, login);
        query.setString(2, password);
        return query.send();
    }

    /**
     * Table user JOIN roles.
     */
    public ResultSet getUserByKeyAndValue(String key, String value) {
        createConnect();
        query.createPreparedQuery("SELECT * " +
                "FROM Users " +
                "WHERE " + key + " = ?");
        query.setString(1, value);
        return query.send();
    }

    /**
     * Create user
     */
    public void createUser(User user) {
        createConnect();
        query.createPreparedQuery("INSERT INTO Users " +
                "VALUES (?, (SELECT uuid FROM Roles WHERE name = 'user'), ?, ?, '0')");
        query.setString(1, UUID.randomUUID().toString());
        query.setString(2, user.getLogin());
        query.setString(3, user.getPassword());
        query.update();
    }

    /**
     * Update role
     */
    public void updateUserRole(User user, Role newRoles) {
        createConnect();
        query.createPreparedQuery("UPDATE (" +
                "SELECT * FROM Users " +
                "WHERE users.uuid = ?) " +
                "SET role_uuid = ?");
        query.setString(1, user.getUUID());
        query.setString(2, newRoles.getUUID());
        query.update();
    }

    public void updateUserBanned(User user, int point) {
        createConnect();
        query.createPreparedQuery("UPDATE Users " +
                "SET is_banned = ? " +
                "WHERE uuid = ?");
        query.setInt(1, point);
        query.setString(2, user.getUUID());
        query.update();
    }
}
