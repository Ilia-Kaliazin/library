package com.epam.library.dao;

import com.epam.library.model.Bookmark;
import com.epam.library.model.User;

import java.sql.ResultSet;
import java.util.UUID;

public class Bookmarks extends DAO {
    private static Bookmarks instance;

    private Bookmarks() {
    }

    public static Bookmarks getInstance() {
        if (instance == null) return instance = new Bookmarks();
        return instance;
    }

    public ResultSet getBookmarkByKeyAndValue(String key, String value) {
        createConnect();
        query.createPreparedQuery("SELECT * " +
                "FROM bookmarks bm JOIN books b on bm.book_uuid = b.uuid JOIN users u on bm.user_uuid = u.uuid " +
                "WHERE " + key + " = ? ");
        query.setString(1, value);
        return query.send();
    }

    public ResultSet getBookmarkUserByKeyAndValue(User user, String key, String value) {
        createConnect();
        query.createPreparedQuery("SELECT * " +
                "FROM bookmarks bm JOIN books b on bm.book_uuid = b.uuid JOIN users u on bm.user_uuid = u.uuid " +
                "WHERE u.uuid = ? " +
                "AND " + key + " = ?");
        query.setString(1, user.getUUID());
        query.setString(2, value);
        return query.send();
    }

    public ResultSet getBookmarkByInfo(Bookmark bookmark) {
        createConnect();
        query.createPreparedQuery("SELECT * " +
                "FROM bookmarks bm JOIN books b on bm.book_uuid = b.uuid JOIN users u on bm.user_uuid = u.uuid " +
                "WHERE user_uuid = ? " +
                "AND book_uuid = ? " +
                "AND page = ?");
        query.setString(1, bookmark.getUser().getUUID());
        query.setString(2, bookmark.getBook().getUUID());
        query.setInt(3, bookmark.getPage());
        return query.send();
    }

    public ResultSet getBookmarksByFilter(User user, String preparedQuery) {
        createConnect();
        query.createPreparedQuery("SELECT * " +
                "FROM bookmarks bm JOIN books b on bm.book_uuid = b.uuid " +
                "JOIN authors a on b.author_uuid = a.uuid " +
                "JOIN users u on bm.user_uuid = u.uuid " +
                "WHERE u.uuid = ? " +
                "AND bm.is_hidden = '0'" +
                preparedQuery);
        query.setString(1, user.getUUID());
        return query.send();
    }

    public void createBookmark(Bookmark bookmark) {
        createConnect();
        query.createPreparedQuery("INSERT INTO Bookmarks " +
                "VALUES (?, ?, (SELECT uuid FROM Books WHERE uuid LIKE ?), ?, '0')");
        query.setString(1, String.valueOf(UUID.randomUUID()));
        query.setString(2, bookmark.getUser().getUUID());
        query.setString(3, bookmark.getBook().getUUID() + "%");
        query.setInt(4, bookmark.getPage());
        query.execute();
    }

    public void updateMark(Bookmark bookmark, String tag, String newValue) {
        createConnect();
        query.createPreparedQuery("UPDATE Bookmarks " +
                "SET " + tag + " = ? " +
                "WHERE uuid = ?");
        query.setString(1, newValue);
        query.setString(2, bookmark.getUUID());
        query.execute();
    }
}
