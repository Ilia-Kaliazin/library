package com.epam.library.dao;

import com.epam.library.model.User;

import java.sql.Date;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.UUID;

public class Logs extends DAO {
    private static Logs instance;

    private Logs() {
    }

    public static Logs getInstance() {
        if (instance == null) return instance = new Logs();
        return instance;
    }

    public void request(User user, String table, String field, String type) {
        createConnect();
        query.createPreparedQuery("INSERT INTO Logs " +
                "VALUES (?, ?, ?, ?, ?, ?)");
        query.setString(1, UUID.randomUUID().toString());
        query.setString(2, user.getUUID());
        query.setString(3, table);
        query.setString(4, field);
        query.setString(5, type);
        query.setDate(6, Date.valueOf(LocalDate.now()));
        query.execute();
    }

    public void request(String userUUID, String table, String field, String type){
        createConnect();
        query.createPreparedQuery("INSERT INTO Logs " +
                "VALUES (?, ?, ?, ?, ?, ?)");
        query.setString(1, UUID.randomUUID().toString());
        query.setString(2, userUUID);
        query.setString(3, table);
        query.setString(4, field);
        query.setString(5, type);
        query.setDate(6, Date.valueOf(LocalDate.now()));
        query.execute();
    }

    public ResultSet getLogByUUID(String uuid) {
        createConnect();
        query.createPreparedQuery("SELECT * " +
                "FROM logs " +
                "WHERE uuid = ? ");
        query.setString(1, uuid);
        return query.send();
    }

    public ResultSet getLogsByUserLogin(String login) {
        createConnect();
        query.createPreparedQuery("SELECT * " +
                "FROM logs l JOIN Users u on l.user_uuid = u.uuid " +
                "WHERE u.login LIKE ? " +
                "ORDER BY request_date ASC");
        query.setString(1, login + "%");
        return query.send();
    }

    public ResultSet getBooksByFilter(String login, String preparedQuery) {
        createConnect();
        query.createPreparedQuery("SELECT * " +
                "FROM logs l JOIN Users u on l.user_uuid = u.uuid " +
                "WHERE u.login LIKE ?" +
                preparedQuery +
                " ORDER BY request_date ASC");
        query.setString(1, login + "%");
        return query.send();
    }
}
