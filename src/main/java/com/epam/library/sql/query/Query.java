package com.epam.library.sql.query;

import java.sql.*;

public class Query {
    private final Connection connection;
    private PreparedStatement preparedStatement;

    public Query(Connection connection) {
        this.connection = connection;
    }

    public ResultSet send() {
        try {
            return preparedStatement.executeQuery();
        } catch (SQLException ignore) {}
        close();
        return null;
    }

    public boolean execute() {
        try {
            return preparedStatement.execute();
        } catch (SQLException ignore) {}
        close();
        return false;
    }

    public void update() {
        try {
          preparedStatement.executeUpdate();
        } catch (SQLException ignore) {}
        close();
    }

    public void createPreparedQuery(String string) {
        try {
            preparedStatement = connection.prepareStatement(string, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        } catch (SQLException ignore) {
        }
    }

    public void setString(int i, String string) {
        try {
            preparedStatement.setString(i, string);
        } catch (SQLException ignored) {}
    }

    public void setInt(int i, int j) {
        try {
            preparedStatement.setInt(i, j);
        } catch (SQLException ignored) {}
    }

    public void setDate(int i, Date j) {
        try {
            preparedStatement.setDate(i, j);
        } catch (SQLException ignored) {}
    }

    public void close() {
        try {
            preparedStatement.close();
            connection.close();
        } catch (SQLException ignored) {}
    }
}
