package com.epam.library.sql;

import com.epam.library.configuration.Config;
import com.epam.library.sql.query.Query;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Compound {
    private String urlSql;
    private String sqlUser;
    private String sqlPassword;

    public Query query;
    // Table
    private static Compound instance;

    private Compound(){}

    public static Compound getInstance() {
        if (instance == null) return instance = new Compound();
        return instance;
    }

    private void getDriver() {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void connectedSql(Connection connection) {
        query = new Query(connection);
    }

    public void connect() {
        try {
            if (urlSql == null || sqlUser == null || sqlPassword == null) return;
            // Methods
            getDriver();
            // Connection
            connectedSql(DriverManager.getConnection(urlSql, sqlUser, sqlPassword));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void uploadDataSQL(Config config) {
        urlSql = config.getValueForTag("URLSQL");
        sqlUser = config.getValueForTag("SQLUSER");
        sqlPassword = config.getValueForTag("SQLPASSWORD");
    }

    public void uploadDataSQL(String urlSql, String sqlUser, String sqlPassword) {
        this.urlSql = urlSql;
        this.sqlUser = sqlUser;
        this.sqlPassword = sqlPassword;
    }

    public void close(){
        query.close();
    }
}
