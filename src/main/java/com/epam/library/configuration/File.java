package com.epam.library.configuration;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class File {
    private java.io.File file;
    private Properties properties;
    private FileInputStream fileInputStream;

    private void initProperties() {
        try {
            fileInputStream = new FileInputStream(file);
            properties = new Properties(System.getProperties());
            properties.load(fileInputStream);
        } catch (IOException ignore) {
        }
    }

    /**
     * Получение статуса файла.
     */
    private boolean getStatusFile() {
        return file.isFile();
    }

    /**
     * Создание файла.
     */
    private void createNewFile() {
        try {
            file.createNewFile();
        } catch (IOException ignored) {
        }
    }

    /**
     * Создание файла при его отсутствии.
     */
    private void checkFile() {
        if (!getStatusFile()) {
            createNewFile();
        }
    }

    public String getValueForTag(String string) {
        if (file == null) return null;
        checkFile();
        initProperties();
        return properties.getProperty(string);
    }

    public void setFile(String string) {
        this.file = new java.io.File(string);
    }

    public void close(){
        try {
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
