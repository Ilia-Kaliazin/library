package com.epam.library.configuration;

public class Config {
    private final File file;
    private static Config instance;

    private Config() {
        file = new File();
    }

    public static Config getInstance() {
        if (instance == null) return instance = new Config();
        return instance;
    }

    public String getValueForTag(String string) {
        return file.getValueForTag(string);
    }

    public void setPathProperties(String path) {
        file.setFile(path);
    }

    public void close(){
        file.close();
    }
}
