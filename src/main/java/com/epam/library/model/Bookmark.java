package com.epam.library.model;

public class Bookmark {
    private String uuid;
    private int page;
    private boolean hidden;
    private User user;
    private Book book;

    public Bookmark() {
    }

    public Bookmark(String uuid, int page, boolean hidden, User user, Book book) {
        this.uuid = uuid;
        this.page = page;
        this.hidden = hidden;
        this.user = user;
        this.book = book;
    }

    public String getUUID() {
        return uuid;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public boolean isHidden() {
        return hidden;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
