package com.epam.library.model;

import java.sql.Date;

public class Log {
    private String uuid;
    private User user;
    private String table;
    private String field;
    private String type;
    private Date date;

    public Log(String uuid, User user, String table, String field, String type, Date date) {
        this.uuid = uuid;
        this.user = user;
        this.table = table;
        this.field = field;
        this.type = type;
        this.date = date;
    }

    public String getUUID() {
        return uuid;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTable() {
        return table;
    }

    public String getField() {
        return field;
    }

    public String getType() {
        return type;
    }

    public Date getDate() {
        return date;
    }

}
