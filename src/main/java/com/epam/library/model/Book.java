package com.epam.library.model;

public class Book {
    private String uuid;
    private String bookName;
    private int releaseYear;
    private int pageCount;
    private String isbn;
    private String publisher;
    private Author author;
    private boolean hidden;

    public Book() {
    }

    public Book(String uuid) {
        this.uuid = uuid;
    }

    public Book(String uuid, String bookName, int releaseYear, int pageCount, String isbn, String publisher, Author author, boolean hidden) {
        this.uuid = uuid;
        this.bookName = bookName;
        this.releaseYear = releaseYear;
        this.pageCount = pageCount;
        this.isbn = isbn;
        this.publisher = publisher;
        this.author = author;
        this.hidden = hidden;
    }

    public String getUUID() {
        return uuid;
    }

    public void setUUID(String uuid) {
        this.uuid = uuid;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public String getISBN() {
        return isbn;
    }

    public void setISBN(String isbn) {
        this.isbn = isbn;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public boolean isHidden() {
        return hidden;
    }
}
