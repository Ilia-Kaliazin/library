package com.epam.library.model;

public enum Role {
    USER,
    MODER,
    ADMIN;

    private String uuid;

    public String getUUID() {
        return uuid;
    }

    public String getName() {
        return this.name();
    }

    public void setUUID(String string) {
        this.uuid = string;
    }
}
