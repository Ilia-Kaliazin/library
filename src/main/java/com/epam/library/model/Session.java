package com.epam.library.model;

public class Session {
    private String uuid;
    private User user;
    private String token;

    public Session(String uuid, User user, String token) {
        this.uuid = uuid;
        this.user = user;
        this.token = token;
    }

    public String getUUID() {
        return uuid;
    }

    public void setUUID(String uuid) {
        this.uuid = uuid;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
