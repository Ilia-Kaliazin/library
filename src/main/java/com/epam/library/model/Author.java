package com.epam.library.model;


import java.sql.Date;

public class Author {
    private String uuid;
    private String name;
    private String secondName;
    private String lastName;
    private Date dob;
    private boolean hidden;

    public Author() {
    }

    public Author(String uuid, String name, String secondName, String lastName, Date dob, boolean hidden) {
        this.uuid = uuid;
        this.name = name;
        this.secondName = secondName;
        this.lastName = lastName;
        this.dob = dob;
        this.hidden = hidden;
    }

    public String getUUID() {
        return uuid;
    }

    public void setUUID(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public boolean isHidden() {
        return hidden;
    }
}
