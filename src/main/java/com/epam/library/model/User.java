package com.epam.library.model;

public class User {
    private String uuid;
    private Role role;
    private String login;
    private String password;
    private boolean banned;

    public User() {
    }

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public User(String uuid, Role role, String login, String password, boolean banned) {
        this.uuid = uuid;
        this.role = role;
        this.login = login;
        this.password = password;
        this.banned = banned;
    }

    public String getUUID() {
        return uuid;
    }

    public void setUUID(String uuid) {
        this.uuid = uuid;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public boolean isBanned() {
        return banned;
    }
}
