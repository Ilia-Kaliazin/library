package com.epam.library.exceptions;

public class BookmarkIncorrectException extends Exception {
    public BookmarkIncorrectException() {
    }

    public BookmarkIncorrectException(String message) {
        super(message);
    }

    public BookmarkIncorrectException(String message, Throwable cause) {
        super(message, cause);
    }

    public BookmarkIncorrectException(Throwable cause) {
        super(cause);
    }
}
