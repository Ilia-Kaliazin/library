package com.epam.library.exceptions;

public class SessionIncorrectException extends Exception {
    public SessionIncorrectException() {
        super();
    }

    public SessionIncorrectException(String message) {
        super(message);
    }

    public SessionIncorrectException(String message, Throwable cause) {
        super(message, cause);
    }

    public SessionIncorrectException(Throwable cause) {
        super(cause);
    }
}
