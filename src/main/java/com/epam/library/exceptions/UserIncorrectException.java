package com.epam.library.exceptions;

public class UserIncorrectException extends Exception {
    public UserIncorrectException() {
    }

    public UserIncorrectException(String message) {
        super(message);
    }

    public UserIncorrectException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserIncorrectException(Throwable cause) {
        super(cause);
    }
}
