package com.epam.library.exceptions;

public class LoginBusyException extends Exception {
    public LoginBusyException() {
        super();
    }

    public LoginBusyException(String message) {
        super(message);
    }

    public LoginBusyException(String message, Throwable cause) {
        super(message, cause);
    }

    public LoginBusyException(Throwable cause) {
        super(cause);
    }

    protected LoginBusyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
