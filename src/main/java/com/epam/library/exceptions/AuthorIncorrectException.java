package com.epam.library.exceptions;

public class AuthorIncorrectException extends Exception {
    public AuthorIncorrectException() {
    }

    public AuthorIncorrectException(String message) {
        super(message);
    }

    public AuthorIncorrectException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthorIncorrectException(Throwable cause) {
        super(cause);
    }
}
