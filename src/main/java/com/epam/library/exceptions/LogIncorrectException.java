package com.epam.library.exceptions;

public class LogIncorrectException extends Exception {
    public LogIncorrectException() {
        super();
    }

    public LogIncorrectException(String message) {
        super(message);
    }

    public LogIncorrectException(String message, Throwable cause) {
        super(message, cause);
    }

    public LogIncorrectException(Throwable cause) {
        super(cause);
    }

    protected LogIncorrectException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
