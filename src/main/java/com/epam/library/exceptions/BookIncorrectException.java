package com.epam.library.exceptions;

public class BookIncorrectException extends Exception {
    public BookIncorrectException() {
    }

    public BookIncorrectException(String message) {
        super(message);
    }

    public BookIncorrectException(String message, Throwable cause) {
        super(message, cause);
    }

    public BookIncorrectException(Throwable cause) {
        super(cause);
    }
}
