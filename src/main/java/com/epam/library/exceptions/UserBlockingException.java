package com.epam.library.exceptions;

public class UserBlockingException extends Exception {
    public UserBlockingException() {
        super();
    }

    public UserBlockingException(String message) {
        super(message);
    }

    public UserBlockingException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserBlockingException(Throwable cause) {
        super(cause);
    }

    protected UserBlockingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
