package com.epam.library.exceptions;

public class BookmarkExceedPagesException extends Exception {
    public BookmarkExceedPagesException() {
        super();
    }

    public BookmarkExceedPagesException(String message) {
        super(message);
    }

    public BookmarkExceedPagesException(String message, Throwable cause) {
        super(message, cause);
    }

    public BookmarkExceedPagesException(Throwable cause) {
        super(cause);
    }

    protected BookmarkExceedPagesException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
