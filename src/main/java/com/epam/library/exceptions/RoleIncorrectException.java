package com.epam.library.exceptions;

public class RoleIncorrectException extends Exception {
    public RoleIncorrectException() {
        super();
    }

    public RoleIncorrectException(String message) {
        super(message);
    }

    public RoleIncorrectException(String message, Throwable cause) {
        super(message, cause);
    }

    public RoleIncorrectException(Throwable cause) {
        super(cause);
    }

    protected RoleIncorrectException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
