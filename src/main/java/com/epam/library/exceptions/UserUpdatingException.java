package com.epam.library.exceptions;

public class UserUpdatingException extends Exception {
    public UserUpdatingException() {
        super();
    }

    public UserUpdatingException(String message) {
        super(message);
    }

    public UserUpdatingException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserUpdatingException(Throwable cause) {
        super(cause);
    }

    protected UserUpdatingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
