package com.epam.library.exceptions;

public class ParseParameterException extends Exception {
    public ParseParameterException() {
        super();
    }

    public ParseParameterException(String message) {
        super(message);
    }

    public ParseParameterException(String message, Throwable cause) {
        super(message, cause);
    }

    public ParseParameterException(Throwable cause) {
        super(cause);
    }

    protected ParseParameterException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
